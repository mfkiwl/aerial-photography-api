from django.urls import path, include
from rest_framework import routers
from api import views

router = routers.DefaultRouter()

router.register(r'unpaged_flight_runs', views.UnpagedFlightRunViewSet)
router.register(r'flight_runs', views.FlightRunViewSet)
router.register(r'unpaged_images', views.UnpagedAerialImageViewSet)
router.register(r'images', views.AerialImageViewSet)
router.register(r'status', views.StatusViewSet, basename="status")
router.register(r'clients', views.ClientViewSet)
router.register(r'custodians', views.CustodianViewSet)
router.register(r'camera_types', views.CameraTypeViewSet)


urlpatterns = [
    path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='restframework'))

    # path('ping', views.ping, name="ping"),
    path('cust_flight_run/<int:frid>', views.flight_run_detail, name="flight_run_detail"),
]


