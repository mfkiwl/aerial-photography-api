import datetime
import re
from django.contrib.gis.gdal import SpatialReference
from django.contrib.gis.geos import Point, Polygon
from aerial_data.models import ANGLE_OBLIQUE, ANGLE_VERTICAL, \
    FS_UNKNOWN, FS_COLOUR, FS_PANCHROMATIC, FS_INFRARED, FlightRun


class BadParameters(Exception):
    def __init__(self, errors):
        self.errors = errors


def parse_bbox_parameter(params, errors):
    # parse CRS if provided
    bb_crs_s = params.get("bbox-crs", "http://www.opengis.net/def/crs/OGC/1.3/CRS84")
    try:
        crs = SpatialReference(bb_crs_s)
    except Exception:
        errors["bbox-crs"] = "bbox-crs does not contain a valid coordinate reference system in a standard format."
        return None

    bbox_raw = params.get("bbox", None)
    bbox = None
    if bbox_raw is None:
        return None
    if isinstance(bbox_raw, str):
        if bbox_raw == "":
            return None
        lin = bbox_raw.split(",")
        if len(lin) == 4:
            try:
                bbox = list([ float(l) for l in lin ])
            except ValueError:
                pass
    else:
        try:
            l = len(bbox_raw)
            if l == 4:
                bbox = list([ float(l) for l in bbox_raw ])
        except Exception:
            pass
    if not bbox:
        errors["bbox"] = "bbox search parameter must be a comma-separated list of coordinates in the order west,south,east,north"
        return None

    # Got 4 numbers!
    w, s, e, n = bbox

    if w >= e or s >= n:
        errors["bbox"] = "bbox search parameter must be a comma-separated list of coordinates in the order west,south,east,north"
        return None

    # TODO - this will break for geographic CRS's in GDAL3
    point_nw = Point(w, n, srid=crs.srid)
    point_sw = Point(w, s, srid=crs.srid)
    point_se = Point(e, s, srid=crs.srid)
    point_ne = Point(e, n, srid=crs.srid)

    poly = Polygon([point_nw, point_sw, point_se, point_ne, point_nw], srid=crs.srid)
    try:
        poly.transform(3577)
        return poly
    except Exception:
        errors["bbox"] = "Could not transform bounding box coordinates to GDA-94 coordinate system.  Please ensure your bounding box coordinates are restricted to Australia and nearby regions."
        return None


def parse_date_andor_time(input):
    iso_dt_re = "(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})(?P<time>T[0-9:.Z+-]*)?"
    iso_t_re  = "T(?P<hour>\d{2}):(?P<min>\d{2}):(?P<sec>\d{2})(?P<fracsec>.\d+)?(?P<timezone>[Z+-][0-9:]*)?"
    iso_tz_re = "(?P<sign>[+-])(?P<houroff>\d{2})(:(?P<minoff>\d{2}))?"

    dt_match = re.fullmatch(iso_dt_re, input)
    if not dt_match:
        return None

    year = int(dt_match.group("year"))
    month = int(dt_match.group("month"))
    day = int(dt_match.group("day"))
    time_str = dt_match.group("time")
    if time_str:
        t_match = re.fullmatch(iso_t_re, time_str)
        if not t_match:
            return None
        hour = int(t_match.group("hour"))
        minute = int(t_match.group("hour"))
        second = int(t_match.group("sec"))
        frac_s = t_match.group("fracsec")
        if frac_s:
            microsecond = int(float(frac_s) * 1000000)
        else:
            microsecond = 0
        tz_s = t_match.group("timezone")
        if not tz_s:
            tz = None
        elif tz_s == "Z":
            tz = datetime.timezone.utc
        else:
            tz_match = re.fullmatch(iso_tz_re, tz_s)
            if not tz_match:
                return None
            signoff = tz_match.group("sign")
            houroff = int(tz_match.group("houroff"))
            minoff  = tz_match.group("minoff")
            if minoff:
                minoff = int(minoff)
            else:
                minoff = 0
            offset = datetime.timedelta(hours=houroff, minutes=minoff)
            if signoff == "-":
                offset = -offset
            tz = datetime.timezone(offset)
    else:
        hour = 0
        minute = 0
        second = 0
        microsecond = 0
        tz = None

    return datetime.datetime(year, month, day, hour, minute, second, microsecond, tzinfo=tz)


def parse_dt_period(input):
    dtp_re = "P(?P<year>\d{4}Y)?(?P<month>\d{2}M)?(?P<day>\d{2}D)?(?P<timepart>T[0-9A-Z]*)?"
    tp_re = "T(?P<hour>\d{2}H)?(?P<minute>\d{2}M)?(?P<second>\d{2}S)?"
    dtp_match = re.fullmatch(dtp_re, input)
    if not dtp_match:
        return None

    y = dtp_match.group("year")
    m = dtp_match.group("month")
    d = dtp_match.group("day")
    timepart = dtp_match.group("timepart")
    if y:
        # TODO
        return None
    if m:
        # TODO
        return None
    if d:
        day = int(d[:-1])
    else:
        day = 0
    if timepart:
        tp_match = re.fullmatch(tp_re, timepart)
        if not tp_match:
            return None
        h = tp_match.group("hour")
        m = tp_match.group("minute")
        s = tp_match.group("second")
        if h:
            hour = int(h)
        else:
            hour = 0
        if m:
            minute = int(m)
        else:
            minute = 0
        if s:
            second = int(s)
        else:
            second = 0
    else:
        hour = 0
        minute = 0
        second = 0
    return datetime.timedelta(days=day, hours=hour, minutes=minute, seconds=second)


def parse_time_parameter(params, errors):
    dts = params.get("time", None)
    if dts is None:
        return None
    bits = dts.split("/")
    if len(bits) > 2:
        errors["time"] = "Time parameter must be a RFC-3399 compliant datetime or datetime period."
        return None
    start_dt = parse_date_andor_time(bits[0])
    if not start_dt:
        errors["time"] = "Time parameter must be a RFC-3399 compliant datetime or datetime period. Note that 'periods to' are not yet supported (only 'periods from')"
        return None
    if len(bits) == 1:
        end_dt = start_dt + datetime.timedelta(1)
    else:
        end_dt = parse_date_andor_time(bits[1])
        if not end_dt:
            period = parse_dt_period(bits[1])
            if period is None:
                errors["time"] = "Time parameter must be a RFC-3399 compliant datetime or datetime period. Note that year, month and week periods are not yet supported."
                return None
            end_dt = start_dt + period
    return (start_dt, end_dt)


def parse_client_parameter(params, errors):
    return params.get("client", None)


def parse_custodian_parameter(params, errors):
    return params.get("custodian", None)


def parse_film_number_parameter(params, errors):
    return params.get("film_number", None)


def parse_barcode_parameter(params, errors):
    return params.get("barcode", None)


def parse_nla_bib_id_parameter(params, errors):
    return params.get("nla_bib_id", None)


def parse_digitised_parameter(params, errors):
    dig = params.get("digitised", None)
    if dig is None:
        return None
    dig = dig.upper()
    if dig == "":
        return None
    elif dig in ("1", "TRUE", "T", "Y", "YES"):
        return True
    elif dig in ("0", "FALSE", "F", "N", "NO"):
        return False

    errors["digitised"] = "Unrecognised value '%s'.  Suggest 'Y' or '1' for true and 'N' or '0' for false." % dig
    return None


def parse_orientation(params, errors):
    o = params.get("orientation", None)
    if o is None:
        return None
    o = o.upper()
    if o in ("V", "VERT", "VERTICAL"):
        return ANGLE_VERTICAL
    elif o in ("O", "OBL", "OBLIQUE"):
        return ANGLE_OBLIQUE

    errors["orientation"] = "Unrecognised value '%s'.  Suggest 'V' vertical or 'O' oblique." % o
    return None


def parse_film_spectrum(params, errors):
    o = params.get("spectrum", None)
    if o is None:
        return None
    o = o.upper()
    if o in ("U", "UNK", "UNKNOWN"):
        return FS_UNKNOWN
    elif o in ("C", "COL", "COLOR", "COLOUR"):
        return FS_COLOUR
    elif o in ("BW", "B/W", "B&W", "BLACK&WHITE", "BLACK/WHITE", "BLACK AND WHITE", "BLACK & WHITE",
               "P", "PAN", "PANCHROMATIC"):
        return FS_PANCHROMATIC
    elif o in ("I", "IR", "INFRARED", "INFRA-RED"):
        return FS_INFRARED

    errors["spectrumm"] = "Unrecognised value %s.  Suggest 'U' (Unknown), 'C' (Colour), 'BW' (Black and White/Panchromatic) or 'I' (Infra-red)" % o
    return None


def parse_site_parameter(params, errors):
    site_param = params.get("site", None)
    if site_param is not None:
        site_param = site_param.upper()
    return site_param


def parse_cam_type_parameter(params, errors):
    return params.get("camera_type", None)

def parse_flightline_parameter(params, errors):
    id = params.get("flightline")
    if id is None:
        return None
    try:
        return FlightRun.objects.get(id=id)
    except FlightRun.DoesNotExist:
        errors["flightline"] = "Flightline does not exist"
        return None


def parse_project_parameter(params, errors):
    proj = params.get("project")
    if proj is not None:
        proj = proj.upper()
    return proj
