from collections import OrderedDict

from django.shortcuts import render
from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.db.models import Q
from rest_framework import viewsets, status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from api.serializers import *
from aerial_data.models import *
from api.api_utils import *

# Create your views here.

def examples(request):
    return render(request, "api_examples.html", {})


def flight_run_detail(request, frid):
    format = request.GET.get("layout")
    if format is None or format == "embedded":
        serializer = FlightRunSerializerEmbedded
    elif format == "geojson":
        serializer = FlightRunSerializerGeoJSON
    else:
        raise HttpResponseBadRequest("Unsupported format: %s" % format)

    try:
        fr = FlightRun.objects.get(id=frid)
    except FlightRun.DoesNotExist:
        raise Http404("Flight Run %d does not exist" % frid)

    try:
        frs = serializer(fr)
        json = JSONRenderer().render(frs.data)
    except NoGeometryException:
        raise HttpResponseBadRequest("This Flight Run has no path geometry and therefore cannot be rendered as GeoJSON")

    return HttpResponse(json, content_type="application/json")

@api_view(["GET"])
def ping(request):
    try:
        nc = Camera.objects.all().count()
        return Response({"type": "pong", "db": "up"}, status=status.HTTP_200_OK)
    except Exception:
        return Response({"type": "pong", "db": "down"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class FlightRunViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FlightRun.objects.all().order_by("spreadsheet_line")
    paged = True

    def get_serializer_class(self):
        layout = self.request.query_params.get("layout", "embedded")
        if layout == "geojson":
            return FlightRunSerializerGeoJSON
        else:
            return FlightRunSerializerEmbedded

    def list(self, request, *args, **kwargs):
        try:
            super_result = super().list(request, *args, **kwargs)
            layout = self.request.query_params.get("layout", "embedded")
            if layout == "geojson":
                if self.paged:
                    super_result.data["results"] = {
                        "type": "FeatureCollection",
                        "crs": {
                            "type": "name",
                            "properties": {
                                "name": "EPSG:%d" % FlightRun._meta.get_field("flight_path").srid
                            }
                        },
                        "features": [
                            f for f in super_result.data["results"] if f is not None
                        ]
                    }
                else:
                    super_result.data = OrderedDict([
                        ("type", "FeatureCollection"),
                        ("crs", {
                                   "type": "name",
                                   "properties": {
                                       "name": "EPSG:%d" % FlightRun._meta.get_field("flight_path").srid
                                   }
                               }),
                        ("features", super_result.data),
                    ])
            return super_result
        except BadParameters as bp:
            return Response(bp.errors, status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        qs = FlightRun.objects.all()
        param_errors = {}
        if self.action == 'list':
            bbox = parse_bbox_parameter(self.request.query_params, param_errors)
            if bbox:
                qs = qs.filter(flight_path__intersects=bbox)
            date_range = parse_time_parameter(self.request.query_params, param_errors)
            if date_range:
                from_date, to_date = date_range
                qs = qs.filter(dates__from_date__lt=to_date, dates__to_date__gte=from_date)
            client_fragment = parse_client_parameter(self.request.query_params, param_errors)
            if client_fragment:
                qs = qs.filter(project__client__icontains=client_fragment)
            custodian_fragment = parse_custodian_parameter(self.request.query_params, param_errors)
            if custodian_fragment:
                qs = qs.filter(film_meta__custodian__icontains=custodian_fragment)
            film_number_fragment = parse_film_number_parameter(self.request.query_params, param_errors)
            if film_number_fragment:
                qs = qs.filter(film__number__icontains=film_number_fragment)
            barcode_fragment = parse_barcode_parameter(self.request.query_params, param_errors)
            if barcode_fragment:
                qs = qs.filter(film_meta__barcode__icontains=barcode_fragment)
            site_fragment = parse_site_parameter(self.request.query_params, param_errors)
            if site_fragment:
                qs = qs.filter(Q(site__site__icontains=site_fragment) | Q(site__geo_area__icontains=site_fragment))
            digitised = parse_digitised_parameter(self.request.query_params, param_errors)
            if digitised is not None:
                if digitised:
                    qs = qs.exclude(film_meta__film_digitised=FILM_DIG_NOT)
                else:
                    qs = qs.filter(film_meta__film_digitised=FILM_DIG_NOT)
            nla_bib_id_fragment = parse_nla_bib_id_parameter(self.request.query_params, param_errors)
            if nla_bib_id_fragment:
                qs = qs.filter(diagram__nla_bib_id__icontains=nla_bib_id_fragment)
            cam_type_fragment = parse_cam_type_parameter(self.request.query_params, param_errors)
            if cam_type_fragment:
                qs = qs.filter(cam__cam_type__icontains=cam_type_fragment)
            orient = parse_orientation(self.request.query_params, param_errors)
            if orient:
                qs = qs.filter(angle=orient)
            spectrum = parse_film_spectrum(self.request.query_params, param_errors)
            if spectrum:
                qs = qs.filter(film_type__spectrum=spectrum)
            layout = self.request.query_params.get("layout", "embedded")
            if layout == "geojson":
                qs = qs.filter(flight_path__isnull=False)
            proj = parse_project_parameter(self.request.query_params, param_errors)
            if proj:
                qs = qs.filter(project__project__icontains=proj)
        if param_errors:
            raise BadParameters(param_errors)
        return qs.order_by("spreadsheet_line")


class UnpagedFlightRunViewSet(FlightRunViewSet):
    pagination_class = None
    paged = False


class StatusViewSet(viewsets.ViewSet):
    @action(detail=False, methods=["get"], name="ping")
    def ping(self, request):
        try:
            nc = Camera.objects.all().count()
            return Response({"type": "pong", "db": "up"}, status=status.HTTP_200_OK)
        except Exception:
            return Response({"type": "pong", "db": "down"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ClientViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Project.objects.all().distinct("client").order_by("client")
    serializer_class = ClientSerializer

    def list(self, request, *args, **kwargs):
        try:
            return super().list(request, *args, **kwargs)
        except BadParameters as bp:
            return Response(bp.errors, status.HTTP_400_BAD_REQUEST)


class CustodianViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FilmMetaData.objects.all().distinct("custodian").order_by("custodian")
    serializer_class = CustodianSerializer

    def list(self, request, *args, **kwargs):
        try:
            return super().list(request, *args, **kwargs)
        except BadParameters as bp:
            return Response(bp.errors, status.HTTP_400_BAD_REQUEST)


class CameraTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Camera.objects.all().distinct("cam_type").order_by("cam_type")
    serializer_class = CameraTypeSerializer

    def list(self, request, *args, **kwargs):
        try:
            return super().list(request, *args, **kwargs)
        except BadParameters as bp:
            return Response(bp.errors, status.HTTP_400_BAD_REQUEST)


class AerialImageViewSet(viewsets.ReadOnlyModelViewSet):
    paged = True
    queryset = AerialImage.objects.all().order_by("flight_run", "id")
    serializer_class = AerialImageSerializerEmbedded

    def list(self, request, *args, **kwargs):
        try:
            super_result = super().list(request, *args, **kwargs)
            layout = self.request.query_params.get("layout", "embedded")
            if layout == "geojson":
                if self.paged:
                    super_result.data["results"] = {
                        "type": "FeatureCollection",
                        "crs": {
                            "type": "name",
                            "properties": {
                                "name": "EPSG:%d" % AerialImage._meta.get_field("centroid").srid
                            }
                        },
                        "features": [
                            f for f in super_result.data["results"] if f is not None
                        ]
                    }
                else:
                    super_result.data = OrderedDict([
                        ("type", "FeatureCollection"),
                        ("crs", {
                            "type": "name",
                            "properties": {
                                "name": "EPSG:%d" % AerialImage._meta.get_field("centroid").srid
                            }
                        }),
                        ("features", super_result.data),
                    ])
            return super_result
        except BadParameters as bp:
            return Response(bp.errors, status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        qs = AerialImage.objects.all()
        param_errors = {}
        if self.action == 'list':
            flight_run = parse_flightline_parameter(self.request.query_params, param_errors)
            if flight_run:
                qs = qs.filter(flight_run=flight_run)
            bbox = parse_bbox_parameter(self.request.query_params, param_errors)
            if bbox:
                qs = qs.filter(centroid__intersects=bbox)
            date_range = parse_time_parameter(self.request.query_params, param_errors)
            if date_range:
                from_date, to_date = date_range
                qs = qs.filter(dates__from_date__lt=to_date, dates__to_date__gte=from_date)
            client_fragment = parse_client_parameter(self.request.query_params, param_errors)
            if client_fragment:
                qs = qs.filter(project__client__icontains=client_fragment)
            custodian_fragment = parse_custodian_parameter(self.request.query_params, param_errors)
            if custodian_fragment:
                qs = qs.filter(film_meta__custodian__icontains=custodian_fragment)
            film_number_fragment = parse_film_number_parameter(self.request.query_params, param_errors)
            if film_number_fragment:
                qs = qs.filter(film__number__icontains=film_number_fragment)
            barcode_fragment = parse_barcode_parameter(self.request.query_params, param_errors)
            if barcode_fragment:
                qs = qs.filter(film_meta__barcode__icontains=barcode_fragment)
            site_fragment = parse_site_parameter(self.request.query_params, param_errors)
            if site_fragment:
                qs = qs.filter(Q(site__site__icontains=site_fragment) | Q(site__geo_area__icontains=site_fragment))
            digitised = parse_digitised_parameter(self.request.query_params, param_errors)
            if digitised is not None:
                if digitised:
                    qs = qs.exclude(film_meta__film_digitised=FILM_DIG_NOT)
                else:
                    qs = qs.filter(film_meta__film_digitised=FILM_DIG_NOT)
            nla_bib_id_fragment = parse_nla_bib_id_parameter(self.request.query_params, param_errors)
            if nla_bib_id_fragment:
                qs = qs.filter(diagram__nla_bib_id__icontains=nla_bib_id_fragment)
            cam_type_fragment = parse_cam_type_parameter(self.request.query_params, param_errors)
            if cam_type_fragment:
                qs = qs.filter(cam__cam_type__icontains=cam_type_fragment)
            orient = parse_orientation(self.request.query_params, param_errors)
            if orient:
                qs = qs.filter(angle=orient)
            spectrum = parse_film_spectrum(self.request.query_params, param_errors)
            if spectrum:
                qs = qs.filter(film_type__spectrum=spectrum)
            proj = parse_project_parameter(self.request.query_params, param_errors)
            if proj:
                qs = qs.filter(project__project__icontains=proj)
        if param_errors:
            raise BadParameters(param_errors)
        return qs.order_by("flight_run", "id")


class UnpagedAerialImageViewSet(AerialImageViewSet):
    pagination_class = None
    paged = False


