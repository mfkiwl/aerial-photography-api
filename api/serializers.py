from aerial_data.models import *
from rest_framework import serializers

class DatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectDates
        fields = [
            "from_date", "to_date", "date_type"
        ]

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = [
            "project", "client", "flown_by"
        ]

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = [ "client" ]


class CustodianSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilmMetaData
        fields = [ "custodian" ]


class SiteSerializer(serializers.ModelSerializer):
    states = serializers.ListField()
    class Meta:
        model = Site
        fields = [
            "site", "geo_area", "states"
        ]

class MilMapSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = MapSheetReference
        fields = [ "map_num" ]


class CivilianMapSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = MapSheetReference
        fields = [ "map_num", "map_name" ]

class MapSheetSerializer(serializers.ModelSerializer):
    map_sheets_250k_military = MilMapSheetSerializer(many=True)
    map_sheets_250k = CivilianMapSheetSerializer(many=True)
    map_sheets_100k = CivilianMapSheetSerializer(many=True)
    map_sheets_50k = CivilianMapSheetSerializer(many=True)
    class Meta:
        model = MapRefs
        fields = [
            "map_sheets_250k_military",
            "map_sheets_250k",
            "map_sheets_100k",
            "map_sheets_50k",
        ]


class CamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Camera
        fields = [
            "brand", "cam_type", "series", "desc", "lens", "focal_len", "digital_camera"
        ]


class CameraTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Camera
        fields = [
            "cam_type",
        ]


class MappedChoiceField(serializers.Field):
    def __init__(self, choices, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.choices = choices
        self.choices_dict = dict(choices)

    def to_representation(self, value):
        return self.choices_dict[value]


class FilmTypeSerializer(serializers.ModelSerializer):
    film_type = MappedChoiceField(choices=film_type_choice)
    spectrum = MappedChoiceField(choices=film_spectrum_choice)
    class Meta:
        model = FilmType
        fields = [
            "film_type",
            "make",
            "spectrum",
            "type_desc"
        ]

class FilmMetaDataSerializer(serializers.ModelSerializer):
    film_digitised = MappedChoiceField(choices=film_digitised_choices)
    class Meta:
        model = FilmMetaData
        fields = [
            "film_digitised",
            "barcode", "barcode_comments",
            "custodian"
        ]


class ScaleField(serializers.Field):
    def to_representation(self, value):
        return "1:%d" % value


class GeomSerializer(serializers.ModelSerializer):
    scale = ScaleField()
    class Meta:
        model = ImageGeometry
        fields = [
            "height_feet", "height_desc", "scale",
            "min_fwd_overlap", "max_fwd_overlap", "fwd_overlap_desc"
        ]


class FlightLineDiagramSerializer(serializers.ModelSerializer):
    aircraft_control_type = MappedChoiceField(choices=aircraft_control_choices)
    aircraft_lift_type = MappedChoiceField(choices=aircraft_lift_choices)
    class Meta:
        model = FlightLineDiagram
        fields = [
            "fld_comments", "ga_imgname", "pms_imgname", "nla_bib_id", "aad_fld_id",
            "aircraft_control_type", "aircraft_lift_type"
        ]


class ScanningMetaDataSerializer(serializers.ModelSerializer):
    derivative = MappedChoiceField(choices=scan_derivative_choices)
    file_purpose = MappedChoiceField(choices=scan_purpose_choices)
    scan_file_type = MappedChoiceField(choices=image_fmt_choices)
    class Meta:
        model = ScanningMetaData
        fields = [
            "scanner", "derivative", "file_purpose", "scan_file_type",
            "capture_ratio", "manipulation", "compression",
            "resolution_microns", "resolution_ppi",
            "bit_depth", "colourspace"
        ]


class FlightRunSerializerBase(serializers.ModelSerializer):
    dates = DatesSerializer()
    project = ProjectSerializer()
    site = SiteSerializer()
    maps = MapSheetSerializer()
    cam = CamSerializer()
    film = serializers.SlugRelatedField(slug_field="number", read_only=True)
    film_type = FilmTypeSerializer()
    geom = GeomSerializer()
    diagram = FlightLineDiagramSerializer()
    film_meta = FilmMetaDataSerializer()
    class Meta:
        model = FlightRun
        fields = [ "url", "dates", "project",
                   "maps", "site",
                   "cam", "film", "film_type", "film_meta",
                   "spreadsheet_line", "run", "angle",
                   "geom", "diagram",
                   "single_frame", "start_frame", "end_frame", "total_photos",
                   "image_url",
                   "flight_path" ]


class AerialImageSerializerBase(serializers.ModelSerializer):
    dates = DatesSerializer()
    project = ProjectSerializer()
    site = SiteSerializer()
    maps = MapSheetSerializer()
    cam = CamSerializer()
    film = serializers.SlugRelatedField(slug_field="number", read_only=True)
    film_type = FilmTypeSerializer()
    geom = GeomSerializer()
    diagram = FlightLineDiagramSerializer()
    scanning_process = ScanningMetaDataSerializer()
    flight_run = serializers.HyperlinkedRelatedField(read_only=True, view_name="flightrun-detail")
    angle = MappedChoiceField(choices=angle_choices)
    image_type = MappedChoiceField(choices=image_type_choices)
    film_meta = FilmMetaDataSerializer()

    class Meta:
        model = AerialImage
        fields = [ "url", "dates", "project",
                   "maps", "site",
                   "cam", "film", "film_type", "film_meta",
                   "run", "angle",
                   "geom", "diagram", "scanning_process",
                   "flight_run",
                   "run", "angle", "image_type", "frame_number",
                   "image_url", "thumbnail_image_url",
                   "centroid", "footprint",
        ]


class FlightRunSerializerEmbedded(FlightRunSerializerBase):
    def to_representation(self, instance):
        raw = super().to_representation(instance)
        # Convert to GeoJSON
        geom_wkt = raw.pop("flight_path")
        if geom_wkt:
            geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "MultiLineString",
                    "coordinates": instance.flight_path_geographic().coords,
                },
            }
        else:
            geojson = None
        raw["flight_path"] = geojson
        return raw


class AerialImageSerializerEmbedded(AerialImageSerializerBase):
    def to_representation(self, instance):
        raw = super().to_representation(instance)
        # Convert to GeoJSON
        geom_wkt = raw.pop("centroid")
        if geom_wkt:
            centroid_geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": instance.centroid_geographic().coords,
                }
            }
        else:
            centroid_geojson = None

        geom_wkt = raw.pop("footprint")
        if geom_wkt:
            footprint_geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": instance.footprint_geographic().coords,
                }
            }
        else:
            footprint_geojson = None

        raw["centroid"] = centroid_geojson
        raw["footprint"] = footprint_geojson
        return raw


class NoGeometryException(Exception):
    pass


class FlightRunSerializerGeoJSON(FlightRunSerializerBase):
    def to_representation(self, instance):
        raw = super().to_representation(instance)
        # Convert to GeoJSON
        geom_wkt = raw.pop("flight_path")
        if not geom_wkt:
            return None
        geojson = {
            "type": "Feature",
            "geometry": {
                "type": "MultiLineString",
                "coordinates": instance.flight_path_geographic().coords,
            },
            "properties": raw
        }
        return geojson


class AerialImageSerializerGeoJSON(AerialImageSerializerBase):
    def to_representation(self, instance):
        raw = super().to_representation(instance)
        # Convert to GeoJSON
        geom_wkt_footprint = raw.pop("footprint")
        geom_wkt_centroid = raw.pop("centroid")
        if not geom_wkt_footprint and not geom_wkt_centroid:
            raise NoGeometryException("AerialImage has no geometry: Cannot render as GeoJSON.")
        if geom_wkt_footprint and geom_wkt_centroid:
            geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "GeometryCollection",
                    "geometries": [
                        {
                            "type": "MultiPolygon",
                            "coordinates": instance.footprint_geographic().coords,
                        },
                        {
                            "type": "Point",
                            "coordinates": instance.centroid_geographic().coords,
                        }
                    ]
                },
                "properties": raw
            }
        elif geom_wkt_footprint:
            geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": instance.footprint_geographic().coords,
                },
                "properties": raw
            }
        else:
            geojson = {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": instance.centroid_geographic().coords,
                },
                "properties": raw
            }
        return geojson


