# Generated by Django 2.1.7 on 2019-10-28 03:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aerial_data', '0002_auto_20190920_0439'),
    ]

    operations = [
        migrations.CreateModel(
            name='AWSFilmLookup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('film_number', models.CharField(max_length=120, unique=True)),
                ('aws_abbrev', models.CharField(max_length=10)),
                ('aws_film_folder', models.CharField(max_length=120)),
                ('license', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='AWSFlightLineDiagramLookup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fld_filename', models.CharField(max_length=256, unique=True)),
                ('aws_folder', models.CharField(max_length=128)),
                ('license', models.CharField(max_length=128)),
            ],
        ),
        migrations.AlterField(
            model_name='aerialimage',
            name='image_type',
            field=models.IntegerField(choices=[(0, 'Raw Image Scan'), (0, 'Raw Image Scan'), (1, 'Geo-Referenced Image'), (2, 'Ortho-Rectified Geo-Referenced Image'), (3, 'Not digitised yet - on archival film only.')]),
        ),
        migrations.AlterField(
            model_name='scanningmetadata',
            name='derivative',
            field=models.IntegerField(choices=[(3, 'Not yet scanned'), (0, 'Raw digital surrogate'), (1, 'Archival master'), (2, 'Digital access')]),
        ),
        migrations.AlterField(
            model_name='scanningmetadata',
            name='file_purpose',
            field=models.IntegerField(choices=[(0, 'Preservation'), (1, 'Access'), (2, 'N/A')]),
        ),
    ]
