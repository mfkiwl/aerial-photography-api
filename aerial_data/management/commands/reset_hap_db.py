import csv
import os

from django.core.management.base import BaseCommand, CommandError

from aerial_data.models import *


class Command(BaseCommand):
    help ="""Delete all db records loaded from a spreadsheet."""

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]

        self.delete_records(AerialImageMosaic)
        self.delete_records(AerialImage)
        self.delete_records(ScanningMetaData)
        self.delete_records(FlightRun)
        self.delete_records(FilmMetaData)
        self.delete_records(FlightLineDiagram)
        self.delete_records(ImageGeometry)
        self.delete_records(Film)
        self.delete_records(FilmType)
        self.delete_records(Camera)
        self.delete_records(Site)
        self.delete_records(MapRefs)
        self.delete_records(MapSheetReference)
        self.delete_records(Project)
        self.delete_records(ProjectDates)


    def delete_records(self, mdl):
        results = mdl.objects.all().delete()
        if self.verbosity > 1:
            self.stdout.write(self.style.SUCCESS("Deleted %s:  %s" % (mdl.__name__, repr(results))))



