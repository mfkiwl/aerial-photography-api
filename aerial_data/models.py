from django.contrib.gis.db import models

# Create your models here.


class PoolHashModel(models.Model):
    poolhash = models.DecimalField(max_digits=19, decimal_places=0, db_index=True)
    class Meta:
        abstract=True


DEFINITE_DMY = 1
DEFINITE_MY  = 2
DEFINITE_Y   = 3
NEEDS_CONFIRMATION = 4
DMY_NEEDS_CONFIRMATION = 5
MY_NEEDS_CONFIRMATION = 6
UNKNOWN = 0
date_type_choices = [
    (UNKNOWN, "Unknown"),
    (DEFINITE_DMY, "Definite day, month & year"),
    (DEFINITE_MY, "Definite month & year"),
    (DEFINITE_Y, "Definite year"),
    (NEEDS_CONFIRMATION, "Needs confirmation"),
    (DMY_NEEDS_CONFIRMATION, "Day, month & year known, but uncertain"),
    (MY_NEEDS_CONFIRMATION, "Month and year known, but uncertain")
]


class ProjectDates(PoolHashModel):
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    date_desc = models.CharField(max_length=100, null=True, blank=True)
    date_type = models.IntegerField(null=True, blank=True, choices=date_type_choices)


class Project(PoolHashModel):
    project = models.CharField(max_length=255, null=True, blank=True)
    client = models.CharField(max_length=255, null=True, blank=True)
    flown_by = models.CharField(max_length=255, null=True, blank=True)

    explorable_fields = ["project", "client", "flown_by"]


MT_250K_MIL = 1
MT_250K     = 2
MT_100K     = 3
MT_50K      = 4

map_type_choices = [
    (MT_250K_MIL, "250k Scale Military"),
    (MT_250K, "250k Scale"),
    (MT_100K, "100k Scale"),
    (MT_50K, "50k Scale"),
]

map_type_dict = dict(map_type_choices)

class MapSheetReference(models.Model):
    map_num = models.CharField(max_length=100)
    map_name = models.CharField(max_length=100, null=True, blank=True)
    map_type = models.IntegerField(choices=map_type_choices)
    # epsg-3577 is GDA-94 (Australian Albers projection)
    footprint = models.PolygonField(srid=3577, blank=True, null=True)
    explorable_fields = ["map_num", "map_name", "map_type"]


class MapRefs(PoolHashModel):
    m250k_mil_list = models.CharField(default="",max_length=1000, blank=True)
    m250k_list = models.CharField(default="",max_length=1000, blank=True)
    m250k_name_list = models.CharField(default="",max_length=1000, blank=True)
    m100k_list = models.CharField(default="",max_length=1000, blank=True)
    m100k_name_list = models.CharField(default="",max_length=1000, blank=True)
    m50k_list = models.CharField(default="",max_length=1000, blank=True)
    m50k_name_list = models.CharField(default="",max_length=1000, blank=True)
    sheets = models.ManyToManyField(MapSheetReference)
    combined_footprint = models.PolygonField(srid=3577, blank=True, null=True)

    def map_sheets_250k_military(self):
        return self.sheets.filter(map_type=MT_250K_MIL)

    def map_sheets_250k(self):
        return self.sheets.filter(map_type=MT_250K)

    def map_sheets_100k(self):
        return self.sheets.filter(map_type=MT_100K)

    def map_sheets_50k(self):
        return self.sheets.filter(map_type=MT_50K)

    explorable_fields = [ "m250k_mil_list",
                          "250k_list", "m250k_name_list",
                          "100k_list", "m100k_name_list",
                          "50k_list", "m50k_name_list"
                          ]


S_UNKNOWN = 0

S_ACT = 1
S_NSW = 2
S_SA  = 3
S_WA  = 4
S_VIC = 5
S_QLD = 6
S_TAS = 7
S_NT  = 8
S_TERR = 9

S_WS = 101
S_TONGA = 102
S_PNG = 103


state_dict = {
    S_UNKNOWN: "Unknown",

    S_ACT:  "ACT",
    S_NSW:  "NSW",
    S_SA:   "SA",
    S_WA:   "WA",
    S_VIC:  "Vic",
    S_QLD:  "Qld",
    S_TAS:  "Tas",
    S_NT:   "NT",
    S_TERR: "Australian Territory",

    S_WS:   "Western Samoa",
    S_TONGA: "Tonga",
    S_PNG: "PNG",
}


state_fields = {
    S_UNKNOWN: "st_unknown",

    S_ACT:  "st_act",
    S_NSW:  "st_nsw",
    S_SA:   "st_sa",
    S_WA:   "st_wa",
    S_VIC:  "st_vic",
    S_QLD:  "st_qld",
    S_TAS:  "st_tas",
    S_NT:   "st_nt",
    S_TERR: "st_terr",

    S_WS:   "st_ws",
    S_TONGA: "st_tonga",
    S_PNG: "st_png",
}


class Site(PoolHashModel):
    site = models.CharField(max_length=255)
    geo_area = models.CharField(max_length=225)
    footprint = models.PolygonField(srid=3577, blank=True, null=True)
    st_unknown = models.BooleanField(default=False)
    st_act = models.BooleanField(default=False)
    st_nsw = models.BooleanField(default=False)
    st_sa = models.BooleanField(default=False)
    st_wa = models.BooleanField(default=False)
    st_vic = models.BooleanField(default=False)
    st_qld = models.BooleanField(default=False)
    st_tas = models.BooleanField(default=False)
    st_nt = models.BooleanField(default=False)
    st_terr = models.BooleanField(default=False)
    st_ws = models.BooleanField(default=False)
    st_tonga = models.BooleanField(default=False)
    st_png = models.BooleanField(default=False)

    def set_state_flag(self, flag):
        setattr(self, state_fields[flag], True)

    def in_state(self, flag):
        return getattr(self, state_fields[flag])

    def st_codes(self):
        codes = []
        for code, fld in state_fields.items():
            if getattr(self, fld):
                codes.append(code)
        return codes

    def states(self):
        return [ state_dict[c] for c in self.st_codes() ]

    def st_display(self):
        names = []
        for code, fld in state_fields.items():
            if getattr(self, fld):
                names.append(state_dict[code])
        return ", ".join(names)

    def save(self, *args, **kwargs):
        if not self.st_codes:
            self.st_unknown = True
        return super().save(*args, **kwargs)

    explorable_fields = ["site", "geo_area"]


class Camera(PoolHashModel):
    brand = models.CharField(max_length=120, null=True, blank=True)
    cam_type = models.CharField(max_length=120, null=True, blank=True)
    series = models.CharField(max_length=120, null=True, blank=True)
    desc = models.CharField(max_length=120, null=True, blank=True)
    lens = models.CharField(max_length=120, null=True, blank=True)
    focal_len = models.CharField(max_length=120, null=True, blank=True)
    digital_camera = models.BooleanField(default=False)

    explorable_fields = ["brand", "cam_type", "series",
                         "desc", "lens", "focal_len"]

FT_UNKNOWN = 0
FT_BW = 1
FT_COLOUR = 2

film_type_choice = (
    (FT_UNKNOWN, "Unknown"),
    (FT_BW, "Black and White"),
    (FT_COLOUR, "Colour"),
)

ft_dict = dict(film_type_choice)
ft_map = {
    "UNKNOWN": FT_UNKNOWN,
    "KODAK 2405XX BLACK/WHITE": FT_BW,
    "BLACK/WHITE": FT_BW,
    "BLACK/WHITE - TOPO BASE": FT_BW,
    "BLACK/WHITE 2402": FT_BW,
    "KODAK 2412 BLACK/WHITE?": FT_BW,
    "COLOUR": FT_COLOUR,
    "COLOUR 2445": FT_COLOUR,
    "COLOUR (SWA)": FT_COLOUR,
    "COLOUR NEGATIVE": FT_COLOUR,
    "COLOUR or BLACK/WHITE": FT_UNKNOWN,
    "BLACK/WHITE?": FT_BW,
    "BLACK/WHITE??": FT_BW,
    "COLOUR?": FT_COLOUR,
    "COLOUR??": FT_COLOUR,
    "COLOUR ATTACHED TO BLACK/WHITE": FT_UNKNOWN,
    "BLACK/WHITE, COLOUR": FT_UNKNOWN,
    "INFRARED COLOUR": FT_COLOUR,
    "INFRARED": FT_UNKNOWN,
    "INFRARED?": FT_UNKNOWN,
}

FS_UNKNOWN = 0
FS_INFRARED = 1
FS_PANCHROMATIC = 2
FS_COLOUR = 3

film_spectrum_choice = (
    (FS_UNKNOWN, "Unknown"),
    (FS_INFRARED, "Infrared"),
    (FS_PANCHROMATIC, "Panchromatic"),
    (FS_COLOUR, "Colour"),
)

fs_dict = dict(film_spectrum_choice)
fs_map = {
    "UNKNOWN": FS_UNKNOWN,
    "NONE": FS_UNKNOWN,
    "INFRARED": FS_INFRARED,
    "INFRARED?": FS_INFRARED,
    "INFRARED COLOUR": FS_INFRARED,
    "PANCHROMATIC": FS_PANCHROMATIC,
    "PAN-X": FS_PANCHROMATIC,
    "NATURAL COLOUR": FS_COLOUR,
    "COLOUR": FS_COLOUR,
    "COLOUR?": FS_COLOUR,
    "COLOUR 2445": FS_COLOUR,
    "COLOUR (SWA)": FS_COLOUR,
    "COLOUR or BLACK/WHITE": FS_UNKNOWN,
    "BLACK/WHITE 2402": FS_UNKNOWN,
    "KODAK 2405XX BLACK/WHITE": FS_PANCHROMATIC,
    "KODAK 2412 BLACK/WHITE": FS_PANCHROMATIC,
    "KODAK 2412 BLACK/WHITE?": FS_PANCHROMATIC,
    "BLACK/WHITE - TOPO BASE": FS_UNKNOWN,
    "BLACK/WHITE": FS_UNKNOWN,
    "BLACK/WHITE?": FS_UNKNOWN,
    "BLACK/WHITE??": FS_UNKNOWN,
}


class FilmType(PoolHashModel):
    film_type = models.IntegerField(choices=film_type_choice, default=FT_UNKNOWN)
    make = models.CharField(max_length=120, null=True, blank=True)
    spectrum = models.IntegerField(choices=film_spectrum_choice, default=FS_UNKNOWN)
    type_desc = models.CharField(max_length=120, null=True, blank=True)

    explorable_fields = [ 'film_type', 'make',
                          'spectrum', 'type_desc'
                        ]


class Film(PoolHashModel):
    number = models.CharField(max_length=120, null=True, blank=True)
    mil_fld_number = models.CharField(max_length=120, null=True, blank=True)


class ImageGeometry(PoolHashModel):
    height_feet = models.IntegerField(null=True, blank=True)
    height_desc = models.CharField(max_length=120, null=True, blank=True)
    scale = models.IntegerField(null=True, blank=True)
    min_fwd_overlap = models.IntegerField(null=True, blank=True)
    max_fwd_overlap = models.IntegerField(null=True, blank=True)
    fwd_overlap_desc = models.CharField(max_length=120, null=True, blank=True)

    explorable_fields = [ "height_feet", "height_desc", "scale",
                          "min_fwd_overlap", "max_fwd_overlap",
                          "fwd_overlap_desc"]


ACTRL_UNKNOWN   = 0
ACTRL_CREWED    = 1
ACTRL_UAV       = 2

ALIFT_UNKNOWN   = 0
ALIFT_AEROSTAT  = 4
ALIFT_FIXEDWING = 8
ALIFT_ROTOR     = 16

aircraft_control_choices = [
    (ACTRL_UNKNOWN, "Unknown"),
    (ACTRL_CREWED, "Crewed"),
    (ACTRL_UAV, "UAV"),
]
aircraft_control_dict = dict(aircraft_control_choices)

aircraft_lift_choices = [
    (ALIFT_UNKNOWN, "Unknown"),
    (ALIFT_AEROSTAT, "Aerostat (hot air balloon, dirigible, etc.)"),
    (ALIFT_FIXEDWING, "Fixed-wing aircraft"),
    (ALIFT_ROTOR, "Rotorcraft"),
]
aircraft_lift_dict = dict(aircraft_lift_choices)

class FlightLineDiagram(PoolHashModel):
    digitised=models.BooleanField()
    digitised_comments=models.CharField(max_length=2000, null=True, blank=True)
    fld_comments=models.CharField(max_length=2001, null=True, blank=True)
    ga_imgname=models.CharField(max_length=250, null=True, blank=True)
    ga_new_imgname=models.CharField(max_length=500, null=True, blank=True)
    pms_equiv=models.BooleanField(null=True, blank=True)
    pms_imgname=models.CharField(max_length=1001, null=True, blank=True)
    nla_bib_id=models.CharField(max_length=200, null=True, blank=True)
    aad_fld_id=models.CharField(max_length=201, null=True, blank=True)
    state_www=models.CharField(max_length=252, null=True, blank=True)
    aircraft_control_type=models.IntegerField(choices=aircraft_control_choices, default=ACTRL_CREWED)
    aircraft_lift_type=models.IntegerField(choices=aircraft_lift_choices, default=ALIFT_FIXEDWING)
    footprint = models.PolygonField(srid=3577, blank=True, null=True)


FILM_DIG_NOT = 0
FILM_DIG_SCANNED = 1
FILM_DIG_GEOREF = 2
FILM_DIG_ORTHORECT = 3
film_digitised_choices = [
    (FILM_DIG_NOT, 0),
    (FILM_DIG_SCANNED, 1),
    (FILM_DIG_GEOREF, 2),
    (FILM_DIG_ORTHORECT, 3),
]
film_digitised_dict = dict(film_digitised_choices)


class FilmMetaData(PoolHashModel):
    film_digitised = models.IntegerField(choices=film_digitised_choices)
    ga_pri_area = models.CharField(max_length=150, null=True, blank=True)
    barcode = models.CharField(max_length=60, null=True, blank=True)
    barcode_comments = models.CharField(max_length=1000, null=True, blank=True)
    custodian = models.CharField(max_length=500, null=True, blank=True)


ANGLE_VERTICAL = 1
ANGLE_OBLIQUE  = 2
angle_choices=[
    (ANGLE_VERTICAL, "Vertical"),
    (ANGLE_OBLIQUE, "Oblique"),
]
angle_dict = dict(angle_choices)


class FlightRun(models.Model):
    dates = models.ForeignKey(ProjectDates, models.CASCADE)
    project = models.ForeignKey(Project, models.CASCADE)
    maps = models.ForeignKey(MapRefs, models.CASCADE)
    site = models.ForeignKey(Site, models.CASCADE)
    cam = models.ForeignKey(Camera, models.CASCADE)
    film = models.ForeignKey(Film, models.CASCADE)
    film_type = models.ForeignKey(FilmType, models.CASCADE)
    geom = models.ForeignKey(ImageGeometry, models.CASCADE)
    diagram = models.ForeignKey(FlightLineDiagram, models.CASCADE)
    film_meta = models.ForeignKey(FilmMetaData, models.CASCADE)

    spreadsheet_line = models.IntegerField(unique=True)
    run = models.CharField(max_length=120, null=True, blank=True)
    angle = models.IntegerField(choices=angle_choices)
    single_frame = models.CharField(max_length=50, null=True, blank=True)
    start_frame = models.CharField(max_length=50,null=True, blank=True)
    end_frame = models.CharField(max_length=50,null=True, blank=True)
    total_photos = models.CharField(max_length=200, null=True, blank=True)
    flight_path = models.MultiLineStringField(srid=3577, blank=True, null=True)

    def create_image(self, frame, ortho, scanning_metadata=None):
        if ortho == IMAGE_TYPE_UNDIGITISED:
            scanning_metadata = get_or_make_unscanned_scan_metadata()
        elif scanning_metadata is None:
            scanning_metadata = get_or_make_default_scan_metadata()
        ai = AerialImage(
            dates=self.dates,
            project=self.project,
            maps=self.maps,
            site=self.site,
            cam=self.cam,
            film=self.film,
            film_type=self.film_type,
            geom=self.geom,
            diagram=self.diagram,
            film_meta=self.film_meta,
            flight_run=self,
            scanning_process=scanning_metadata,
            run=self.run,
            angle=self.angle,
            frame_number=frame,
            image_type=ortho
        )
        ai.save()
        return ai

    def image_url(self):
        for val in (
                self.diagram.ga_new_imgname,
                self.diagram.pms_imgname,
                self.diagram.ga_imgname,
        ):
            try:
                fld_lkup = AWSFlightLineDiagramLookup.objects.get(fld_filename=val)
                raw_url = "https://%s/FlightlineDiagrams/%s/%s" % (
                    "historicalaerialphotography.s3-ap-southeast-2.amazonaws.com",
                    fld_lkup.aws_folder,
                    fld_lkup.fld_filename
                )
                return raw_url.replace(" ", "+")
            except AWSFlightLineDiagramLookup.DoesNotExist:
                pass
        return None

    def flight_path_geographic(self):
        if self.flight_path.srid != 4326:
            self.flight_path.transform("EPSG:4326")
        return self.flight_path

    class Meta:
        indexes = [
            models.Index(fields=["film", "run"]),
            models.Index(fields=["start_frame", "end_frame"]),
        ]


SCAN_DERIV_RAW = 0
SCAN_DERIV_ARCHIVAL_MASTER = 1
SCAN_DERIV_DIGITAL_ACCESS = 2
SCAN_NOT_SCANNED = 3

scan_derivative_choices = [
    (SCAN_NOT_SCANNED, "Not yet scanned"),
    (SCAN_DERIV_RAW, "Raw digital surrogate"),
    (SCAN_DERIV_ARCHIVAL_MASTER, "Archival master"),
    (SCAN_DERIV_DIGITAL_ACCESS, "Digital access"),
]
scan_derivative_dict = dict(scan_derivative_choices)

SCAN_PURPOSE_PRESERVATION = 0
SCAN_PURPOSE_ACCESS = 1
SCAN_PURPOSE_NA = 2

scan_purpose_choices = [
    (SCAN_PURPOSE_PRESERVATION, "Preservation"),
    (SCAN_PURPOSE_ACCESS, "Access"),
    (SCAN_PURPOSE_NA, "N/A"),
]
scan_purpose_dict = dict(scan_purpose_choices)

IMAGE_FMT_UNKNOWN = 0
IMAGE_FMT_TIFF6 = 1
IMAGE_FMT_COG   = 2
IMAGE_FMT_JPG   = 3
IMAGE_FMT_PNG   = 4
IMAGE_FMT_JPG2K   = 5
image_fmt_choices = [
    (IMAGE_FMT_UNKNOWN, "UNKNOWN"),
    (IMAGE_FMT_TIFF6, "TIFF 6.0"),
    (IMAGE_FMT_COG, "Cloud-Optimised GeoTiff"),
    (IMAGE_FMT_JPG, "JPEG"),
    (IMAGE_FMT_PNG, "PNG"),
    (IMAGE_FMT_JPG2K, "JPEG-2000"),
]

class ScanningMetaData(models.Model):
    scanner=models.CharField(max_length=128, null=True, blank=True)
    derivative=models.IntegerField(choices=scan_derivative_choices)
    file_purpose=models.IntegerField(choices=scan_purpose_choices)
    scan_file_type=models.IntegerField(choices=image_fmt_choices)
    capture_ratio=models.CharField(max_length=128, null=True, blank=True)
    manipulation=models.CharField(max_length=512, null=True, blank=True)
    compression=models.CharField(max_length=256, null=True, blank=True)
    resolution_microns=models.DecimalField(max_digits=8, decimal_places=1, null=True, blank=True)
    resolution_ppi=models.DecimalField(max_digits=8, decimal_places=0, null=True, blank=True)
    bit_depth=models.IntegerField(null=True, blank=True)
    colourspace=models.CharField(max_length=128, null=True, blank=True)


def get_or_make_default_scan_metadata():
    return ScanningMetaData.objects.get_or_create(
        scanner=None,
        derivative=SCAN_DERIV_RAW,
        file_purpose=SCAN_PURPOSE_ACCESS,
        scan_file_type=IMAGE_FMT_UNKNOWN,
        capture_ratio=None,
        manipulation=None,
        compression=None,
        resolution_microns=None,
        resolution_ppi=None,
        bit_depth=None,
        colourspace=None
    )[0]


def get_or_make_unscanned_scan_metadata():
    return ScanningMetaData.objects.get_or_create(
        scanner=None,
        derivative=SCAN_NOT_SCANNED,
        file_purpose=SCAN_PURPOSE_NA,
        scan_file_type=IMAGE_FMT_UNKNOWN,
        capture_ratio=None,
        manipulation=None,
        compression=None,
        resolution_microns=None,
        resolution_ppi=None,
        bit_depth=None,
        colourspace=None
    )[0]

IMAGE_TYPE_RAW = 0
IMAGE_TYPE_GEOREF = 1
IMAGE_TYPE_ORTHORECT = 2
IMAGE_TYPE_UNDIGITISED = 3

image_type_choices = [
    (IMAGE_TYPE_RAW, "Raw Image Scan"),
    (IMAGE_TYPE_RAW, "Raw Image Scan"),
    (IMAGE_TYPE_GEOREF, "Geo-Referenced Image"),
    (IMAGE_TYPE_ORTHORECT, "Ortho-Rectified Geo-Referenced Image"),
    (IMAGE_TYPE_UNDIGITISED, "Not digitised yet - on archival film only."),
]
image_type_dict = dict(image_type_choices)


class AerialImage(models.Model):
    dates = models.ForeignKey(ProjectDates, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    maps = models.ForeignKey(MapRefs, on_delete=models.CASCADE)
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    cam = models.ForeignKey(Camera, on_delete=models.CASCADE)
    film = models.ForeignKey(Film, on_delete=models.CASCADE)
    film_type = models.ForeignKey(FilmType, on_delete=models.CASCADE)
    geom = models.ForeignKey(ImageGeometry, on_delete=models.CASCADE)
    diagram = models.ForeignKey(FlightLineDiagram, on_delete=models.CASCADE)
    film_meta = models.ForeignKey(FilmMetaData, on_delete=models.CASCADE)
    flight_run = models.ForeignKey(FlightRun, on_delete=models.CASCADE)
    scanning_process = models.ForeignKey(ScanningMetaData, on_delete=models.CASCADE)

    run = models.CharField(max_length=120, null=True, blank=True)
    angle = models.IntegerField(choices=angle_choices)
    image_type = models.IntegerField(choices=image_type_choices)
    frame_number = models.CharField(max_length=50)
    centroid = models.PointField(srid=3577, blank=True, null=True)
    footprint = models.MultiPolygonField(srid=3577, blank=True, null=True)

    def image_url(self):
        if self.film_meta.film_digitised:
            try:
                film_lkup = AWSFilmLookup.objects.get(film_number=self.film.number)
                raw_url = "https://%s/Scanned/%s/%s/%s_frame%s.tif" % (
                    "historicalaerialphotography.s3-ap-southeast-2.amazonaws.com",
                    film_lkup.aws_abbrev,
                    film_lkup.aws_film_folder,
                    film_lkup.aws_film_folder,
                    self.frame_number,
                )
                return raw_url.replace(" ", "+")
            except AWSFilmLookup.DoesNotExist:
                pass
        return None

    def thumbnail_image_url(self):
        if self.film_meta.film_digitised:
            try:
                film_lkup = AWSFilmLookup.objects.get(film_number=self.film.number)
                raw_url = "https://%s/Scanned/%s/%s/Preview/%s_frame%s_preview.jpg" % (
                    "historicalaerialphotography.s3-ap-southeast-2.amazonaws.com",
                    film_lkup.aws_abbrev,
                    film_lkup.aws_film_folder,
                    film_lkup.aws_film_folder,
                    self.frame_number,
                )
                return raw_url.replace(" ", "+")
            except AWSFilmLookup.DoesNotExist:
                pass
        return None

    def centroid_geographic(self):
        if self.centroid.srid != 4326:
            self.centroid.transform("EPSG:4326")
        return self.centroid

    def footprint_geographic(self):
        if self.footprint.srid != 4326:
            self.footprint.transform("EPSG:4326")
        return self.footprint

    class Meta:
        indexes = [
            models.Index(fields=["flight_run", "frame_number"]),
        ]


class AerialImageMosaic(models.Model):
    dates = models.ForeignKey(ProjectDates, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    maps = models.ForeignKey(MapRefs, on_delete=models.CASCADE)
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    cam = models.ForeignKey(Camera, on_delete=models.CASCADE)
    film_type = models.ForeignKey(FilmType, on_delete=models.CASCADE)
    geom = models.ForeignKey(ImageGeometry, on_delete=models.CASCADE)
    diagram = models.ForeignKey(FlightLineDiagram, on_delete=models.CASCADE)

    footprint = models.PolygonField(srid=3577, blank=True, null=True)

    constituents = models.ManyToManyField(AerialImage, related_name="mosaics")

    def footprint_geographic(self):
        if self.footprint.srid != 4326:
            self.footprint.transform("EPSG:4326")
        return self.footprint


explorable_models = [
    Project, MapRefs,
    Site, Camera, ImageGeometry,
    FilmType,
]


class AWSFlightLineDiagramLookup(models.Model):
    fld_filename = models.CharField(max_length=256, unique=True)
    aws_folder   = models.CharField(max_length=128)
    license      = models.CharField(max_length=128)


class AWSFilmLookup(models.Model):
    film_number = models.CharField(max_length=120, unique=True)
    aws_abbrev  = models.CharField(max_length=10)
    aws_film_folder = models.CharField(max_length=120)
    license      = models.CharField(max_length=128)


