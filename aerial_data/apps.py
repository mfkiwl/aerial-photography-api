from django.apps import AppConfig


class AerialDataConfig(AppConfig):
    name = 'aerial_data'
