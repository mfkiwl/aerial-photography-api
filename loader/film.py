from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import Film, FilmType, ft_map, fs_map


class LdFilmType(PoolableObj):
    mdl = FilmType
    fields = ["type", "make", "spectrum", "type_desc"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.film_type = self.type
        m.make = self.make
        m.type_desc = self.type_desc
        m.spectrum = self.spectrum
        m.save()
        return m


class LdFilm(PoolableObj):
    mdl = Film
    fields = ["number", "mil_fld_number"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.number = self.number
        m.mil_fld_number = self.mil_fld_number
        m.save()
        return m


class FilmTypePool(ObjPool):
    def candidate(self, row):
        return LdFilmType(
            ft_map[row[FILM_TYPE].strip()],
            read_unk_to_none(row, FILM_MAKE),
            fs_map[row[FILM_SPECTRUM].strip()],
            read_unk_to_none(row, FILM_TYPE_DESC)
        )

    def report(self, log):
        log.write("%d distinct film types in pool(%d in database)" % (
            len(self),
            FilmType.objects.all().count()
        ))


class FilmPool(ObjPool):
    def candidate(self, row):
        return LdFilm(
            read_unk_to_none(row, FILM_NUM),
            read_with_smart_null(row, MIL_FLD_NUM, ["", "NONE"])
        )

    def report(self, log):
        log.write("%d distinct films in pool(%d in database)" % (
            len(self),
            Film.objects.all().count()
        ))

