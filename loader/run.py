from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import *

ANGLE_VERTICAL = 1
ANGLE_OBLIQUE  = 2


class APRun(PoolableObj):
    fields = ["run", "frame", "frame_start", "frame_end", "total_photos", "line", "angle"]
    defaults = { "angle": ANGLE_VERTICAL }


class APRunPool(ObjPool):
    def candidate(self, row):
        obl = row[OBLIQUE]
        if obl == "NONE":
            # Vertical
            angle = ANGLE_VERTICAL
            run = read_with_smart_null(row,VERT_RUN, [None,""])
            frm = read_with_smart_null(row,VERT_FRAME, [".","NONE","N/A","?",None,"","UNKNOWN"])
            frm_start = read_with_smart_null(row,VERT_FSTART, ["?",None,"","UNKNOWN"])
            frm_end = read_with_smart_null(row,VERT_FEND, ["?",None,"","UNKNOWN"])
            total_photos = read_with_smart_null(row,VERT_TOTAL, ["?",None,"","UNKNOWN"])
        else:
            # Oblique
            angle = ANGLE_OBLIQUE
            run = read_with_smart_null(row,OBL_RUN, [None,""])
            frm = read_with_smart_null(row,OBL_FRAME, [".","NONE","N/A","?",None,"","UNKNOWN"])
            frm_start = read_with_smart_null(row,OBL_FSTART, ["?",None,"","UNKNOWN"])
            frm_end = read_with_smart_null(row,OBL_FEND, ["?",None,"","UNKNOWN"])
            total_photos = read_with_smart_null(row,OBL_TOTAL, ["?",None,"","UNKNOWN"])
        return APRun(
            run, frm, frm_start, frm_end, total_photos, self.line,
            angle=angle
        )

    def persist(self, data):
        # Note that persist for runs is (VERY) different to other
        # Poolable Objects, coz we're not really pooling.
        # (Assume that all the REALLY poolable objects are persisted.)
        for blob in data:
            if blob["run"].total_photos and blob["run"].total_photos.startswith("SAME "):
                print("Skipping spreadsheet line %d because marked as duplicate (total_photos starts with 'SAME')" % blob["run"].line)
                continue
            try:
                fr = FlightRun.objects.get(spreadsheet_line=blob["run"].line)
            except FlightRun.DoesNotExist:
                fr = FlightRun(spreadsheet_line=blob["run"].line)
            fr.project=blob["proj"].find_model()
            fr.dates=blob["dates"].find_model()
            fr.maps=blob["maps"].find_model()
            fr.site=blob["site"].find_model()
            fr.cam=blob["cam"].find_model()
            fr.film=blob["film"].find_model()
            fr.film_type=blob["film_type"].find_model()
            fr.geom=blob["geom"].find_model()
            fr.diagram=blob["fld"].find_model()
            fr.film_meta=blob["film_meta"].find_model()
            fr.run=blob["run"].run
            fr.angle=blob["run"].angle
            fr.single_frame=blob["run"].frame
            fr.start_frame=blob["run"].frame_start
            fr.end_frame=blob["run"].frame_end
            fr.total_photos=blob["run"].total_photos
            fr.save()

            fr.aerialimage_set.update(
                project=fr.project,
                dates=fr.dates,
                maps=fr.maps,
                site=fr.site,
                cam=fr.cam,
                film=fr.film,
                film_type=fr.film_type,
                geom=fr.geom,
                diagram=fr.diagram,
                film_meta=fr.film_meta,
                run=fr.run,
                angle=fr.angle,
            )

    def report(self, log):
        log.write("%d distinct runs (%d in database)" %
                  (len(self), FlightRun.objects.all().count()))

