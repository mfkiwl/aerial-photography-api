import re

from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import FlightLineDiagram

class FLDiagram(PoolableObj):
    mdl = FlightLineDiagram
    fields = ["digitised", "digitised_comment",
              "fld_comments", "ga_imgname", "ga_new_imgname",
              "pms_equiv", "pms_imgname",
              "nla_bib_id", "aad_fld_id", "state_www"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.digitised = self.digitised
        m.digitised_comments = self.digitised_comment
        m.fld_comments = self.fld_comments
        m.ga_imgname = self.ga_imgname
        m.ga_new_imgname = self.ga_new_imgname
        m.pms_equiv = self.pms_equiv
        m.pms_imgname = self.pms_imgname
        m.nla_bib_id = self.nla_bib_id
        m.aad_fld_id = self.aad_fld_id
        m.state_www = self.state_www
        m.save()
        return m


class FLDPool(ObjPool):
    def candidate(self, row):
        digit_raw = row[FLD_DIGITISED]
        d_bits = digit_raw.split("-", maxsplit=1)
        if d_bits[0].strip() == "YES":
            digitised = True
        else:
            digitised = False
        if len(d_bits) > 1:
            digitised_comment = d_bits[1].strip()
        else:
            digitised_comment = None

        fld_comments = row[FLD_COMMENTS]
        ga_name = row[GA_IMG_NAME]
        new_ga_name = row[NEW_GA_IMG_NAME]
        if ga_name == "NONE":
            ga_name = None
        else:
            ga_name = tuple(ga_name.split(","))
            if len(ga_name) == 1:
                ga_name = ga_name[0]
        if new_ga_name == '?':
            new_ga_name = None
        else:
            new_ga_name = tuple(new_ga_name.split(","))
            if len(new_ga_name) == 1:
                new_ga_name = new_ga_name[0]

        pms_equiv = (row[PMS_EQUIVALENT] == "YES")
        pms_name_raw = row[PMS_IMAGE_NAME]
        if pms_name_raw == "NONE":
            pms_name = None
        else:
            pms_name = tuple([s.strip() for s in re.split("[,&]", pms_name_raw)])
            if len(pms_name) == 1:
                pms_name = pms_name[0]

        nla_bib_id = row[NLA_BIB_ID]
        if nla_bib_id == "UNKNOWN":
            nla_bib_id = None
        aad = row[AAD_FLIGHT_LINE_ID]
        if not aad:
            aad = None
        state_www = row[STATE_GOVT_WWW]
        if not state_www:
            state_www = None

        return FLDiagram(digitised, digitised_comment,
                                 fld_comments, ga_name, new_ga_name,
                                 pms_equiv, pms_name,
                                 nla_bib_id, aad, state_www
                                 )

    def report(self, log):
        log.write("%d distinct diagrams in pool(%d in database)" % (
            len(self),
            FlightLineDiagram.objects.all().count()
        ))

