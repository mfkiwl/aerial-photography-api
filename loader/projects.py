from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import *


class LdProject(PoolableObj):
    mdl = Project
    fields = ["project", "client", "flown_by"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.project = self.project
        m.client = self.client
        m.flown_by = self.flown_by
        m.save()
        return m


class ProjectPool(ObjPool):
    def candidate(self, row):
        proj = row[PROJECT]
        client = row[CLIENT]
        flown_by = row[FLOWN_BY]

        return LdProject(proj, client, flown_by)

    def report(self, log):
        log.write("%d distinct projects in pool(%d in database)" % (
            len(self),
            Project.objects.all().count()
        ))
