from django.contrib import admin
from aerial_data.models import *

# Register your models here.

@admin.register(ProjectDates)
class ProjectDatesAdmin(admin.ModelAdmin):
    list_display = [ 'from_date', 'to_date', 'date_desc' ]

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [ 'project', 'client', 'flown_by' ]

@admin.register(MapRefs)
class MapRefAdmin(admin.ModelAdmin):
    pass

@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass

@admin.register(Camera)
class CameraAdmin(admin.ModelAdmin):
    pass

@admin.register(Film)
class FilmAdmin(admin.ModelAdmin):
    pass

@admin.register(ImageGeometry)
class ImgGeomAdmin(admin.ModelAdmin):
    pass

@admin.register(FlightLineDiagram)
class FLDAdmin(admin.ModelAdmin):
    pass

@admin.register(FilmMetaData)
class FilmMetaAdmin(admin.ModelAdmin):
    pass


