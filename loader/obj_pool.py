

class PoolableObj(object):
    mdl = None

    fields = []
    defaults = {}

    def __init__(self, *args, **kwargs):
        self.model = None
        ingested_fields = set()
        for i in range(len(args)):
            fld = self.fields[i]
            setattr(self, fld, args[i])
            ingested_fields.add(fld)
        for k,v in kwargs.items():
            if k not in self.fields:
                raise Exception("Field %s not defined for %s" % (
                    k, self.__class__.__name__
                ))
            if k in ingested_fields:
                raise Exception("Field %s already processed in %s.__init__" % (
                    k, self.__class__.__name__
                ))
            setattr(self, k, v)
            ingested_fields.add(k)
        for f in self.fields:
            if f not in ingested_fields:
                if f in self.defaults:
                    setattr(self, f, self.defaults[f])
                else:
                    raise Exception("Field %s not passed to %s.__init__" % (
                        k, self.__class__.__name__
                    ))

    def hashable(self):
        return tuple(( getattr(self, f) for f in self.fields))

    def __hash__(self):
        return hash(self.hashable())

    def __eq__(self, other):
        for f in self.fields:
            if getattr(self, f) != getattr(other, f):
                return False
        return True

    def find_model(self):
        if not self.model:
            try:
                self.model = self.mdl.objects.get(poolhash=hash(self))
            except self.mdl.DoesNotExist:
                self.model = self.persist()
        return self.model

    def persist(self):
        raise Exception("Persist method not implemented for %s" % self.__class__.__name__)


class ObjPool(object):
    def __init__(self):
        self.pool = {}
        self.line = 0

    def candidate(self, row):
        raise Exception("Candidate method not implemented for %s" % self.__class__.__name__)

    def process_row(self, row, line_num):
        self.line = line_num
        try:
            candidate = self.candidate(row)
        except Exception as e:
            print("Error (%s) on line %d in pool %s" % (str(e), line_num, self.__class__.__name__))
            raise e
        hc = hash(candidate)
        if hc in self.pool:
            from_pool = self.pool[hc]
            assert from_pool == candidate
            return from_pool
        else:
            self.pool[hc] = candidate
            return candidate

    def __len__(self):
        return len(self.pool)

    def persist(self):
        for p in self.pool.values():
            try:
                p.find_model()
            except Exception as e:
                print("DB merge failed", self.__class__.__name__, e)
    def report(self, log):
        raise Exception("Report method not implemented for", self.__class__.__name__)


def read_with_smart_null(row, field, nulls):
    raw = row[field].strip()
    if raw in nulls:
        return None
    else:
        return raw


def read_unk_to_none(row, field):
    return read_with_smart_null(row, field, ["UNKNOWN"])


def read_with_emptystrings(row, field, nulls):
    raw = row[field].strip()
    if raw is None or raw in nulls:
        return ""
    else:
        return raw


def readlist_with_emptystrings(row, field, nulls):
    raw = row[field].strip()
    if raw is None or raw in nulls:
        return ""
    else:
        splitted = raw.split(",")
        return ",".join(splitted)

