import csv
import os

from django.core.management.base import BaseCommand, CommandError

from aerial_data.models import *


class Command(BaseCommand):
    help ="""Load film lookup data."""

    def add_arguments(self, parser):
        parser.add_argument("csv_filename", help="The name of the film lookup csv file.")

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]
        try:
            with open(options['csv_filename'], newline='') as csvfile:
                self.init_parser(csvfile)
                self.load()
        except FileNotFoundError:
            raise CommandError("File %s does not exist" % options['csv_filename'])
        print("%d lookup entries loaded (%d skipped)" % (self.loaded, self.skipped))

    def init_parser(self, csvfile):
        self.file = csvfile
        self.reader = csv.reader(self.file)

        self.skipped = 0
        self.loaded = 0
        self.line = 0

    def load(self):
        for row in self.reader:
            self.line += 1
            if self.line == 1:
                if row[0] != "FILM_NUMBER":
                    raise CommandError("Header line not found: aborting")
            else:
                try:
                    self.process(row)
                    self.loaded += 1
                except Exception as e:
                    print("Line %d skipped: ", e)
                    self.skipped += 1

    def process(self, row):
        film_num = row[0]
        try:
            lkup = AWSFilmLookup.objects.get(film_number=film_num)
        except AWSFilmLookup.DoesNotExist:
            lkup = AWSFilmLookup(film_number=film_num)
        lkup.aws_abbrev = row[1]
        lkup.aws_film_folder = row[2]
        lkup.license = row[3]
        lkup.save()

