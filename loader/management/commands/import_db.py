import csv
import os

from django.core.management.base import BaseCommand, CommandError

from loader.column_headings import *

from loader.projects import ProjectPool
from loader.project_dates import ProjectDatesPool
from loader.maps import MapRefsPool
from loader.sites import SitePool
from loader.run import APRunPool
from loader.film import FilmPool, FilmTypePool
from loader.cam import CamPool
from loader.geom import GeomPool
from loader.fld import FLDPool
from loader.film_meta import FilmMetadataPool


class Command(BaseCommand):
    help ="""Read an aerial imagery spreadsheet and load/merge data into a database."""

    def add_arguments(self, parser):
        parser.add_argument("csv_filename", help="The name of the aerial imagery database spreadsheet (exported to csv)")

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]
        if "PYTHONHASHSEED" not in os.environ:
            raise CommandError("This command relies on the $PYTHONHASHSEED environment variable which is not set.  Database merging will not work correctly unless this environment variable is set to the same value as previous loads.")
        try:
            with open(options['csv_filename'], newline='') as csvfile:
                self.init_parser(csvfile)
                self.load()
        except FileNotFoundError:
            raise CommandError("File %s does not exist" % options['csv_filename'])

    def init_parser(self, csvfile):
        self.file = csvfile
        self.reader = csv.DictReader(self.file)

        self.skipped = 0
        self.loaded = 0
        self.line = 1

        self.skipped_reasons = {}

        self.dates = ProjectDatesPool()
        self.projects = ProjectPool()
        self.maps = MapRefsPool()
        self.sites = SitePool()
        self.cams = CamPool()
        self.films = FilmPool()
        self.film_types = FilmTypePool()
        self.geoms = GeomPool()
        self.runs = APRunPool()
        self.flds = FLDPool()
        self.film_meta = FilmMetadataPool()

        self.data = []

    def load(self):
        for row in self.reader:
            self.line += 1
            try:
                self.process(row)
            except Exception:
                pass

        print ("Spreadsheet read, persisting...")
        self.dates.persist()
        self.projects.persist()
        self.maps.persist()
        self.sites.persist()
        self.cams.persist()
        self.films.persist()
        self.film_types.persist()
        self.geoms.persist()
        self.flds.persist()
        self.film_meta.persist()

        self.runs.persist(self.data)


        if self.verbosity > 1:
            self.dates.report(self.stdout)
            self.projects.report(self.stdout)
            self.maps.report(self.stdout)
            self.sites.report(self.stdout)
            self.cams.report(self.stdout)
            self.films.report(self.stdout)
            self.film_types.report(self.stdout)
            self.geoms.report(self.stdout)
            self.flds.report(self.stdout)
            self.film_meta.report(self.stdout)
            self.runs.report(self.stdout)
            self.stdout.write("------------------------------------")
        self.stdout.write(self.style.SUCCESS("%d rows loaded successfully" % self.loaded))

        if self.skipped:
            self.stdout.write("")
            self.stdout.write(self.style.WARNING("Skipped rows:"))
            self.stdout.write(self.style.WARNING("============="))
            for reason, lines in self.skipped_reasons.items():
                self.stdout.write(self.style.WARNING("%s: %d rows (first instance on line %d)" % (reason, len(lines), lines[0])))
            self.stdout.write(self.style.WARNING("Total: %d rows skipped" % self.skipped))


    def process(self, row):
        if not row[DATE] and not row[DATE_ALT]:
            self.skip("Summary Row")
            return False

        try:
            self.data.append({
                "dates": self.dates.process_row(row, self.line),
                "proj": self.projects.process_row(row, self.line),
                "maps": self.maps.process_row(row, self.line),
                "site": self.sites.process_row(row, self.line),
                "geom": self.geoms.process_row(row, self.line),
                "film": self.films.process_row(row, self.line),
                "film_type": self.film_types.process_row(row, self.line),
                "cam": self.cams.process_row(row, self.line),
                "run": self.runs.process_row(row, self.line),
                "fld": self.flds.process_row(row, self.line),
                "film_meta": self.film_meta.process_row(row, self.line),
            })
        except Exception as e:
            print ("Error %s on line %d" % (str(e), self.line))
            raise e
            self.skip(str(e))
            return False
        self.loaded += 1
        return True

    def skip(self, reason):
        self.skipped += 1
        if reason not in self.skipped_reasons:
            self.skipped_reasons[reason] = []
        self.skipped_reasons[reason].append(self.line)
        return

