import csv
import os

from django.core.management.base import BaseCommand, CommandError

from aerial_data.models import *


class Command(BaseCommand):
    help ="""Load Flightline Diagram lookup data."""

    def add_arguments(self, parser):
        parser.add_argument("csv_filename", help="The name of the FLD lookup csv file.")

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]
        try:
            with open(options['csv_filename'], newline='') as csvfile:
                self.init_parser(csvfile)
                self.load()
        except FileNotFoundError:
            raise CommandError("File %s does not exist" % options['csv_filename'])
        print("%d lookup entries loaded (%d skipped)" % (self.loaded, self.skipped))

    def init_parser(self, csvfile):
        self.file = csvfile
        self.reader = csv.reader(self.file)

        self.skipped = 0
        self.loaded = 0
        self.line = 0

    def load(self):
        for row in self.reader:
            self.line += 1
            if self.line == 1:
                if row[0] != "FLIGHT_LINE_DIAGRAM":
                    raise CommandError("Header line not found: aborting")
            else:
                try:
                    self.process(row)
                    self.loaded += 1
                except Exception as e:
                    print("Line %d skipped: ", e)
                    self.skipped += 1

    def process(self, row):
        fld_fname = row[0]
        try:
            lkup = AWSFlightLineDiagramLookup.objects.get(fld_filename=fld_fname)
        except AWSFlightLineDiagramLookup.DoesNotExist:
            lkup = AWSFlightLineDiagramLookup(fld_filename=fld_fname)
        lkup.aws_folder = row[1]
        lkup.license = row[2]
        lkup.save()

