from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import FilmNegativeRegister


class FilmNegReg(PoolableObj):
    mdl = FilmNegativeRegister
    fields = ["fnr_ctr_no",
              "fnr_foclen_mm", "fnr_height_m",
              "fnr_comments", "fnr_wide_angle_page"]
    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.container_number = self.fnr_ctr_no
        m.focal_length_mm = self.fnr_foclen_mm
        m.height_m = self.fnr_height_m
        m.comments = self.fnr_comments
        m.wide_angle_page = self.fnr_wide_angle_page
        m.save()
        return m


class FNRPool(ObjPool):
    def candidate(self, row):
        ctr_no = row[FNR_CTR_NUM]
        if not ctr_no or ctr_no == "UNKNOWN":
            ctr_no = None
        foclen_mm = row[FNR_LENS_FOCAL_LEN]
        if not foclen_mm:
            foclen_mm = None
        height_m = row[FNR_HEIGHT_M]
        if height_m in ("", "UNKNOWN", "NO"):
            height_m = None
        comments = row[FNR_COMMENTS]
        if not comments:
            comments = None
        wide_angle = row[WIDE_ANGLE_FNR_REG_PAGE]
        if wide_angle in ("", "UNKNOWN", "NO"):
            wide_angle = None
        try:
            if height_m is not None:
                hm = int(height_m)
        except:
            print("FNR height_m invalid on line ", self.line, repr(height_m))
        try:
            if wide_angle is not None:
                hm = int(wide_angle)
        except:
            print("FNR wide_angle invalid on line ", self.line, repr(wide_angle))
        return FilmNegReg(
            ctr_no, foclen_mm, height_m, comments, wide_angle)

    def report(self, log):
        log.write("%d distinct film negative register entries in pool(%d in database)" % (
            len(self),
            FilmNegativeRegister.objects.all().count()
        ))

