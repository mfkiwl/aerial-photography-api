from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import ImageGeometry


class Geom(PoolableObj):
    mdl = ImageGeometry
    fields = [ "height_feet", "height_desc", "scale",
               "min_fwd_overlap", "max_fwd_overlap", "fwd_overlap_desc"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.height_feet = self.height_feet
        m.height_desc = self.height_desc
        m.scale = self.scale
        m.min_fwd_overlap = self.min_fwd_overlap
        m.max_fwd_overlap = self.max_fwd_overlap
        m.fwd_overlap_desc = self.fwd_overlap_desc
        m.save()
        return m

problem_vals = [
    '13000 feet',
    '70% (88mm)',
    'UNKNOWN',
    'UNKNOWN BUT MIGHT BE 34000',
    '4500, 5000',
    '4000, 14000',
    '18840, 18900',
    '18840, 18900',
    '4000 to 14000',
    '14500 (',
    '256.02',
    'VARIOU',
    '',
    '17000 (APPROX ONLY',
    'NOMINAL SCALE 1:8000',
    '10000 (',
]


class GeomPool(ObjPool):
    def candidate(self, row):
        hght_ft_orig = row[HEIGHT_FT]
        if hght_ft_orig in [
            "",
            "UNKNOWN", "N/A",
            "VARIOUS",
            "VARIOUS - 3000 to E of Sheet"]:
            hght_ft = None
        elif hght_ft_orig.endswith(" mtres"):
            hght_metres = int(hght_ft_orig[:-6])
            hght_ft = str(int(hght_metres * 3.283))
        else:
            hght_ft = hght_ft_orig
        if hght_ft is not None:
            hght_ft = hght_ft.rstrip('fetAMGSL? ')
            hf_split = hght_ft.split(",")
            if len(hf_split)  > 1:
                hght_ft = hf_split[0]
            hf_split = hght_ft.split("to")
            if len(hf_split)  > 1:
                hght_ft = hf_split[0]
            hf_split = hght_ft.split(".")
            if len(hf_split) == 2:
                hght_ft = hf_split[0]
            if hght_ft in problem_vals:
                print("Height feet lost: ", repr(hght_ft_orig), "->", repr(hght_ft))
            try:
                hfi = int(hght_ft)
            except:
                raise Exception("Height feet %s mapped to %s - but still not an int" % (hght_ft_orig, hght_ft))
        scale_orig = row[SCALE]
        if scale_orig in [
            "UNKNOWN", "UNKNOWN?", "N/A", "?",
            "L-LEVEL OBLIQUE", "S/E",
            "VARIOUS",
            "13000 feet",
            ]:
            scale = None
        else:
            scale = scale_orig
            scale = scale.rstrip('()SWAPOXNLYIMTVERG? ')
            scale = scale.strip('RF.APROX ')
            sc_split = scale.split("or")
            if len(sc_split) > 1:
                scale = sc_split[0].strip()
            sc_split = scale.split("/")
            if len(sc_split) > 1:
                scale = sc_split[1].strip()
            sc_split = scale.split(",")
            if len(sc_split) > 1:
                scale = sc_split[1].strip()
            sc_split = scale.split("to")
            if len(sc_split) > 1:
                scale = sc_split[1].strip()
            if scale.startswith("NOMINAL SCALE 1:"):
                scale = scale[16:]
            if scale == "UNKNOWN BUT MIGHT BE 34000":
                scale = "34000"
            if scale in problem_vals:
                print("Scale lost: ", repr(scale_orig), "->", repr(scale))
            try:
                sci = int(scale)
            except:
                raise Exception("Scale %s mapped to %s - but still not an int" % (scale_orig, scale))


        min_fo = read_with_smart_null(row, MIN_FWD_OVERLAP, [
            "UNKNOWN", "S/E", "S/Es", "N/A", "SINGLE EXPOSURE"
        ])
        if min_fo is not None:
            min_fo = min_fo.strip("%? ")
            if min_fo == "70% (88mm)":
                min_fo = "70"
        if min_fo in problem_vals:
            print("min_fo lost: ", repr(row[MIN_FWD_OVERLAP]), "->", min_fo)

        max_fo = read_with_smart_null(row, MAX_FWD_OVERLAP, [
            "UNKNOWN", "S/E", "S/Es", "N/A", "SINGLE EXPOSURE"
        ])
        if max_fo is not None:
            max_fo = max_fo.strip("%? ")
            if max_fo == "70% (88mm)":
                max_fo = "70"
        if max_fo in problem_vals:
            print("max_fo lost: ", repr(row[MAX_FWD_OVERLAP]), "->", max_fo)

        return Geom(
            hght_ft,
            read_unk_to_none(row, HEIGHT_DESC),
            scale,
            min_fo,
            max_fo,
            read_unk_to_none(row, FWD_OVERLAP_DESC)
        )

    def report(self, log):
        log.write("%d distinct imaging geometries in pool(%d in database)" % (
            len(self),
            ImageGeometry.objects.all().count()
        ))

