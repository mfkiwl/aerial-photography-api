from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import *


class LdSites(PoolableObj):
    mdl = Site
    fields = ["site", "states", "geo_area"]
    def persist(self):
        s = Site()
        s.poolhash = hash(self)
        s.site = self.site
        s.geo_area = self.geo_area
        for state in self.states:
            s.set_state_flag(state)
        s.save()
        return s


state_map = {
    "UNKNOWN": S_UNKNOWN,

    "act": S_ACT,
    "ACT": S_ACT,
    "A.C.T.": S_ACT,
    "Australian Capital Territory": S_ACT,
    "nsw": S_NSW,
    "NSW": S_NSW,
    "New South Wales": S_NSW,
    "SA": S_SA,
    "sa": S_SA,
    "South Australia": S_SA,
    "WA": S_WA,
    "wa": S_WA,
    "Western Australia": S_WA,
    "vic": S_VIC,
    "Vic": S_VIC,
    "VIC": S_VIC,
    "Victoria": S_VIC,
    "qld": S_QLD,
    "Qld": S_QLD,
    "QLD": S_QLD,
    "Queensland": S_QLD,
    "tas": S_TAS,
    "Tas": S_TAS,
    "TAS": S_TAS,
    "Tasmania": S_TAS,
    "nt": S_NT,
    "NT": S_NT,
    "N.T.": S_NT,
    "Northern Territory": S_NT,
    "AUSTRALIAN TERRITORY": S_TERR,
    "AUST TERRITORY": S_TERR,

    "WESTERN SAMOA GOVERNMENT": S_WS,
    "KINGDOM OF TONGA": S_TONGA,
    "PAPUA NEW GUINEA": S_PNG,
}


def parse_state_list(row):
    sl_str = row[STATE]
    sl = []
    sstr_l = sl_str.split(",")
    for ss in sstr_l:
        ss = ss.strip()
        try:
            sl.append(state_map[ss])
        except KeyError:
            raise Exception("Unrecognised state: %s" % ss)
    return tuple(sorted(sl))


class SitePool(ObjPool):
    obj_cls = LdSites
    def candidate(self, row):
        site = row[SITE]
        geo_area = row[GEO_AREA]
        states = parse_state_list(row)

        return LdSites(site, states, geo_area)

    def report(self, log):
        log.write("%d distinct sites in pool(%d in database)" % (
            len(self),
            Site.objects.all().count()
        ))

