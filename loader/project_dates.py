import datetime
import re

from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import *


class LdProjectDates(PoolableObj):
    mdl = ProjectDates
    fields = ["from_date", "to_date", "date_alt", "date_type"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.from_date = self.from_date
        m.to_date = self.to_date
        m.date_desc = self.date_alt
        m.date_type = self.date_type
        m.save()
        return m


date_type_map = {
    "Definite month & year": DEFINITE_MY,
    "Definite day, month & year": DEFINITE_DMY,
    "Definite year": DEFINITE_Y,
    "Date needs confirmation": NEEDS_CONFIRMATION,
    "Definite month & year but need to check date?": MY_NEEDS_CONFIRMATION,
    "Definite day, month & year but may need to check date?": DMY_NEEDS_CONFIRMATION,
    "": UNKNOWN,
}


month_map = {
    "JAN": 1, "JANUARY": 1,
    "FEB": 2, "FEBRUARY": 2,
    "MAR": 3, "MARCH": 3,
    "APR": 4, "APRIL": 4,
    "MAY": 5,
    "JUN": 6, "JUNE": 6,
    "JUL": 7, "JULY": 7,
    "AUG": 8, "AUGUST": 8,
    "SEP": 9, "SEPTEMBER": 9,
    "OCT": 10, "OCTOBER": 10,
    "NOV": 11, "NOVEMBER": 11,
    "DEC": 12, "DECEMBER": 12,
}


class ProjectDatesPool(ObjPool):
    def read_date(self, ds):
        ds = ds.strip()
        if not ds or ds == "UNKNOWN":
            return None
        try:
            return datetime.datetime.strptime(ds, "%d/%m/%Y").date()
        except ValueError:
            pass
        try:
            return datetime.datetime.strptime(ds, "%d/%m/%y").date()
        except ValueError:
            pass
        try:
            y = int(ds)
            return datetime.datetime(y, 1, 1).date()
        except ValueError:
            pass
        return "Invalid date <%s> on line %d" % (ds, self.line)

    def candidate(self, row):
        # from_date = self.read_date(row[FROM_DATE])
        # to_date = self.read_date(row[TO_DATE])
        date_s = row[DATE].strip()
        date_alt_s = row[DATE_ALT].strip()
        if date_alt_s:
            date = "%s, %s" % (date_s, date_alt_s)
        else:
            date = date_s

        try:
            pdate_type = date_type_map[row[DATE_TYPE]]
        except KeyError:
            raise Exception("Invalid date type on line %d" % self.line)

        if self.read_date(date_alt_s) is None:
            date_alt_s = ""

        from_date = self.read_date(date_s)
        if from_date is None:
            to_date = None
            if pdate_type not in (UNKNOWN,NEEDS_CONFIRMATION):
                pdate_type = NEEDS_CONFIRMATION
            if date_alt_s:
                ad = self.read_date(date_alt_s)
                if isinstance(ad, datetime.date):
                    from_date = ad
                    to_date = ad
                else:
                    print("Alt date %s with unknown main date on line %d" % (date_alt_s, self.line))
        elif not isinstance(from_date, datetime.date):
            handled = False
            match = re.match("(?P<dec>[12][0-9][0-9]0)s", date_s)
            if match:
                decade = int(match.group("dec"))
                match = re.match("(?P<dec>[12][0-9][0-9]0)s", date_alt_s)
                if match:
                    alt_dec = int(match.group("dec"))
                else:
                    alt_dec = None
                if alt_dec:
                    start_dec = min(decade, alt_dec)
                    end_dec = max(decade, alt_dec)
                else:
                    start_dec = decade
                    end_dec = decade
                from_date = datetime.date(start_dec, 1, 1)
                to_date = datetime.date(end_dec + 9, 12, 31)
                handled = True
            match = re.match("(?P<date>[0-9]+/[0-9]+/[0-9]+)\?+", date_s)
            if match:
                from_date = self.read_date(match.group("date"))
                if not isinstance(from_date, datetime.date):
                    raise Exception("Couldn't parse queried date on line %d: %s" % (self.line, date_s))
                to_date = from_date
                if date_alt_s:
                    raise Exception("Alt date with queried main date on line %d" % self.line)
                if pdate_type not in (UNKNOWN, NEEDS_CONFIRMATION):
                    pdate_type = NEEDS_CONFIRMATION
                handled=True
            if not handled:
                raise Exception("Can't parse date on line %d: %s" % (self.line, date_s))
        elif date_alt_s:
            handled = False
            if not handled:
                match = re.match("BTN (?P<y1>[0-9]+) \& (?P<y2>[0-9]+)", date_alt_s)
                if match:
                    if pdate_type not in (UNKNOWN,NEEDS_CONFIRMATION):
                        raise Exception("BTN Y & Y format alt date, but date doesn't need confirmation on line %d" % self.line)
                    if from_date.year != int(match.group("y1")):
                        raise Exception("BTN Y & Y format alt date, but date doesn't match start year on line %d" % self.line)
                    to_date = datetime.date(int(match.group("y2")), 12, 31)
                    handled = True
            if not handled:
                alt_date = self.read_date(date_alt_s)
                if isinstance(alt_date, datetime.date):
                    if alt_date != from_date and pdate_type not in (UNKNOWN,NEEDS_CONFIRMATION):
                        y = from_date.year
                        m = from_date.month
                        d = from_date.day
                        ay = alt_date.year
                        am = alt_date.month
                        ad = alt_date.day
                        if y != ay:
                            pdate_type = NEEDS_CONFIRMATION
                        elif m != am:
                            pdate_type = DEFINITE_Y
                        elif d != ad:
                            pdate_type = DEFINITE_DMY
                    if alt_date > from_date:
                        to_date = alt_date
                    else:
                        to_date = from_date
                        from_date = alt_date
                    handled = True
            if not handled:
                match = re.match("(?P<m1>[a-zA-Z]+)-(?P<m2>[a-zA-Z]+) (?P<y>[1-2][0-9][0-9][0-9])", date_alt_s)
                if match:
                    try:
                        m1 = month_map[match.group("m1").upper()]
                        m2 = month_map[match.group("m2").upper()]
                        y = int(match.group("y"))
                    except KeyError:
                        raise Exception("Mon-Mon YYYY format alt date broken on line %d: %s" % (self.line,date_alt_s))
                    if pdate_type != NEEDS_CONFIRMATION:
                        raise Exception("Mon-Mon YYYY format alt date, but date doesn't need confirmation on line %d" % self.line)
                    if from_date != datetime.date(y, m1, 1):
                        raise Exception("Mon-Mon YYYY format alt date, but date doesn't match first month on line %d" % self.line)
                    m2 += 1
                    if m2 > 12:
                        m2 -= 12
                        y += 1
                    to_date = datetime.date(y, m2, 1) - datetime.timedelta(days=1)
                    handled = True
            if not handled:
                match = re.match("(?P<d1>[0-9]+/[0-9]+/[0-9]+)( or |, | & |; )(?P<d2>[0-9]+/[0-9]+/[0-9]+)", date_alt_s)
                if match:
                    d1 = self.read_date(match.group("d1"))
                    d2 = self.read_date(match.group("d2"))
                    if not isinstance(d1, datetime.date) or not isinstance(d2, datetime.date):
                        raise Exception("date-or-date format alt date, with invalid dates on line %d" % self.line)
                    if not isinstance(d1, datetime.date) or not isinstance(d2, datetime.date):
                        raise Exception("date-or-date format alt date, with invalid dates on line %d" % self.line)
                    if pdate_type not in (UNKNOWN, NEEDS_CONFIRMATION):
                        raise Exception("date-or-date format alt date, date type not unknown or needs confirmation on line %d" % self.line)
                    from_date = min(from_date, d1, d2)
                    to_date = max(from_date, d1, d2)
                    handled = True
            if not handled:
                raise Exception("Got an alternate date - not sure how to handle that yet on line %d" % self.line)
        elif pdate_type in (NEEDS_CONFIRMATION, DEFINITE_DMY, DMY_NEEDS_CONFIRMATION):
            to_date = from_date
        elif pdate_type in (DEFINITE_MY, MY_NEEDS_CONFIRMATION):
            y = from_date.year
            m = from_date.month
            from_date = datetime.date(y, m, 1)
            m += 1
            if m > 12:
                m -= 12
                y += 1
            to_date = datetime.date(y, m, 1) - datetime.timedelta(days=1)
        elif pdate_type == DEFINITE_Y:
            to_date = datetime.date(from_date.year, 12, 31)
        elif pdate_type == UNKNOWN and from_date is None and not date_alt_s:
            to_date = None
        else:
            raise Exception("Not sure what to do here either on line %d" % self.line)
        return LdProjectDates(from_date, to_date, date, pdate_type)

    def report(self, log):
        log.write("%d distinct project dates in pool(%d in database)" % (
            len(self),
            ProjectDates.objects.all().count()
        ))
