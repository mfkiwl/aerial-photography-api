from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import FilmMetaData


class LdFilmMetaData(PoolableObj):
    mdl = FilmMetaData
    fields = ["custodian", "film_digitised", "ga_pri_area", "barcode", "barcode_comments"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.film_digitised = self.film_digitised
        m.ga_pri_area = self.ga_pri_area
        m.barcode = self.barcode
        m.barcode_comments = self.barcode_comments
        m.custodian = self.custodian
        m.save()
        return m


class FilmMetadataPool(ObjPool):
    def candidate(self, row):
        film_dig = (row[FILM_DIGITISED] == "YES")
        ga_pri_area = row[GA_PRIORITY_AREA]
        custodian = row[CUSTODIAN]
        if ga_pri_area == "NO":
            ga_pri_area = None
        barcode = row[BARCODE]
        if barcode == "UNKNOWN":
            barcode = None
        barcode_comments = row[BARCODE_COMMENTS]

        return LdFilmMetaData(custodian, film_dig, ga_pri_area, barcode, barcode_comments)

    def report(self, log):
        log.write("%d distinct film metadata in pool(%d in database)" % (
            len(self),
            FilmMetaData.objects.all().count()
        ))

