from django.shortcuts import render, redirect
from django.apps import apps
from aerial_data.models import *


# Create your views here.

def explore_index(request):
    return render(request, "explore/index.html", {
        'models': [ m.__name__ for m in explorable_models ]
    })


def explore_model(request, model):
    try:
        mdl = apps.get_app_config("loader").get_model(model)
    except LookupError:
        return redirect("explore_index")

    total_count = mdl.objects.all().count()
    fields = []
    for fld in mdl.explorable_fields:
        fldinfo = {
            'fld': fld,
            'distinct_vals': mdl.objects.order_by(fld).distinct(fld).count()
        }
        fields.append(fldinfo)
    return render(request, "explore/model.html",
                  {
                    "mdl": mdl.__name__,
                    "fields": fields,
                    "total_count": total_count,
                  })


def explore_field(request, model, field):
    try:
        mdl = apps.get_app_config("loader").get_model(model)
    except LookupError:
        return redirect("explore_index")
    if field not in mdl.explorable_fields:
        return redirect("explore_index")
    distinct_vals = mdl.objects.order_by(field).distinct(field)
    n_distinct_vals = distinct_vals.count()
    vals = []
    for dv in distinct_vals:
        v = getattr(dv, field)
        kwargs = { field: v }
        val = {
            'value': v,
            'count': mdl.objects.filter(**kwargs).count()
        }
        vals.append(val)

    return render(request, "explore/field.html",
                  {
                      "mdl": mdl.__name__,
                      "field": field,
                      "distinct_vals": n_distinct_vals,
                      "vals": vals
                  })




