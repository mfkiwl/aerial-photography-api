from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import Camera

class Cam(PoolableObj):
    mdl = Camera
    fields = [ "brand", "type", "series", "desc", "lens",
               "focal_len"]

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.brand = self.brand
        m.cam_type = self.type
        m.series = self.series
        m.desc = self.desc
        m.lens = self.lens
        m.focal_len = self.focal_len
        m.save()
        return m



CAM_BRAND = "CAMERA BRAND"
CAM_TYPE  = "CAMERA TYPE"
CAM_SERIES = "CAMERA SERIES"
CAM_DESC = "CAMERA DESCRIPTION"
CAM_LENS = "LENS"
CAM_FOCAL_LEN = "FOCAL_LENGTH"


class CamPool(ObjPool):
    def candidate(self, row):
        return Cam(
            read_unk_to_none(row, CAM_BRAND),
            read_unk_to_none(row, CAM_TYPE),
            read_unk_to_none(row, CAM_SERIES),
            read_unk_to_none(row, CAM_DESC),
            read_unk_to_none(row, CAM_LENS),
            read_unk_to_none(row, CAM_FOCAL_LEN),
        )

    def report(self, log):
        log.write("%d distinct cameras in pool(%d in database)" % (
            len(self),
            Camera.objects.all().count()
        ))

