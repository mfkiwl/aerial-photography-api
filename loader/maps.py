from loader.column_headings import *
from loader.obj_pool import *
from aerial_data.models import MapRefs, MapSheetReference, MT_250K_MIL, MT_250K, MT_100K, MT_50K


def null_to_empty_str(sin):
    if sin is None:
        return ""
    else:
        return sin


class LdMapRefs(PoolableObj):
    mdl = MapRefs
    fields = ["m250k_mil", "m250k", "m250k_name",
              "m100k", "m100k_name",
                "m50k", "m50k_name" ]

    meta_map = {
        MT_50K: ("m50k", "m50k_name"),
        MT_100K: ("m100k", "m100k_name"),
        MT_250K: ("m250k", "m250k_name"),
        MT_250K_MIL: ("m250k_mil", None)
    }

    def persist(self):
        m = self.mdl()
        m.poolhash = hash(self)
        m.m250k_mil_list = self.m250k_mil
        m.m250k_list = self.m250k
        m.m250k_name_list = self.m250k_name
        m.m100k_list = self.m100k
        m.m100k_name_list = self.m100k_name
        m.m50k_list = self.m50k
        m.m50k_name_list = self.m50k_name
        m.save()
        self.build_map_refs(m, MT_250K_MIL)
        self.build_map_refs(m, MT_250K)
        self.build_map_refs(m, MT_100K)
        self.build_map_refs(m, MT_50K)
        return m

    def fix_map_refs(self, line, map_type):
        map_fld, map_name_fld = self.meta_map[map_type]
        if not map_name_fld:
            return

        number_list = getattr(self, map_fld)
        if not number_list:
            numbers = []
        else:
            numbers = number_list.split(",")

        name_list = getattr(self, map_name_fld)
        if not name_list:
            names = []
        else:
            names = name_list.split(",")

        if len(numbers) != len(names):
            print("map name/number list mismatch on line %d: number_list: %s name_list: %s -> %s ; %s" % (
                line,
                repr(number_list),
                repr(name_list),
                repr(numbers),
                repr(names)
            ))
            print("\t(Setting to empty map list)")

            setattr(self, map_fld, "")
            setattr(self, map_name_fld, "")


    def build_map_refs(self, mrs, map_type):
        map_fld, map_name_fld = self.meta_map[map_type]
        number_list = getattr(self, map_fld)
        if not number_list:
            numbers = []
        else:
            numbers = number_list.split(",")
        if map_name_fld is None:
            names = [ None ] * len(numbers)
        else:
            name_list = getattr(self, map_name_fld)
            if not name_list:
                names = []
            else:
                names = name_list.split(",")
            if len(numbers) != len(names):
                raise Exception("map name/number list mismatch error number_list: %s name_list: %s -> %s ; %s" % (
                    repr(number_list),
                    repr(name_list),
                    repr(numbers),
                    repr(names)
                ))
        for number, name in zip(numbers, names):
            if number:
                try:
                    msr = MapSheetReference.objects.get(map_num=number, map_name=name,
                                                        map_type=map_type)
                except MapSheetReference.DoesNotExist:
                    msr = MapSheetReference(map_num=number, map_name=name, map_type=map_type)
                    msr.save()
                mrs.sheets.add(msr)


class MapRefsPool(ObjPool):
    def candidate(self, row):
        cand = LdMapRefs(readlist_with_emptystrings(row, MAP_250K_MIL, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_250K_NO, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_250K_NAME, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_100K_NO, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_100K_NAME, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_50K_NO, ["NONE"]),
                            readlist_with_emptystrings(row, MAP_50K_NAME, ["NONE"]),
                            )
        cand.fix_map_refs(self.line, MT_250K)
        cand.fix_map_refs(self.line, MT_100K)
        cand.fix_map_refs(self.line, MT_50K)
        return cand

    def report(self, log):
        log.write("%d distinct map references (%d in database)" % (
            len(self),
            MapRefs.objects.all().count()
        ))

