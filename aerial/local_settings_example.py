from .base_settings import *

# Example local config file.
#
# Make a copy of this file called "local_settings.py" and edit for
# your local deployment.
#
# local_settings.py is in .gitignore, but is assumed to import from
# base_settings and over-ride config entries as needed.
#

# SECRET_KEY
#
# No session authentication is currently used by the api, so
# SECRET_KEY isn't that important yet.  But you should AlWAYS
# override the one in base_settings.py and create a new one
# for each deployment (i.e. for each distinct database
# instance) to prepare for future extensions.
#
# SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = '9c7(i%a2%**%_w@q--nllvyu388m95+)c$9vg#(xt05+48kx@4'

# DEBUG (optional)
#
# If you want to turn on debugging for a deployment, do it here.
#
# SECURITY WARNING: don't run with debug turned on in production!
#
# DEBUG = True

# ALLOWED_HOSTS
#
# List all host names/bare IP addresses that this deployment can
# expect to see requests for.

# For a local dev deployment
ALLOWED_HOSTS = [ 'localhost', '127.0.0.1' ]

# For a production deployment
# ALLOWED_HOSTS = [ 'api.mydomain.org', 'api.aliasdomain.com' ]

# DATABASES
#
# Configure the connection details for your database here.
#
# SECURITY_WARNING: Keep the password for your database secret!

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'HOST': 'mydatabase_server.mydomain.org',
        'PORT': 5432,
        'NAME': 'aerial_proto',
        'USER': 'aerial',
        'PASSWORD': 'Shhh_ItsASecret'
    }
}


