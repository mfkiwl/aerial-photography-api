from .local_settings import *

# local_settings is in .gitignore, but is assumed to import from
# base_settings and over-ride config entries as needed.
#
# At a minimum, local_settings should override:
#
# SECRET_KEY
# ALLOWED_HOSTS
# DATABASES