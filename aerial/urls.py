"""aerial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from loader import views as loader_views
from api import views as api_views

urlpatterns = [
    path('', api_views.examples),
    path('admin/', admin.site.urls),
 #   path('plate/', include('django_spaghetti.urls')),

    path('api/', include('api.urls')),

    path('explore', loader_views.explore_index, name="explore_index"),
    path('explore/<model>', loader_views.explore_model, name="explore_model"),
    path('explore/<model>/<field>', loader_views.explore_field, name="explore_field"),
]
