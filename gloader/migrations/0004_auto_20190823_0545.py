# Generated by Django 2.1.7 on 2019-08-23 05:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gloader', '0003_auto_20190821_0543'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='flightarc',
            index=models.Index(fields=['film_number', 'run'], name='gloader_fli_film_nu_5fa651_idx'),
        ),
        migrations.AddIndex(
            model_name='flightpoint',
            index=models.Index(fields=['arc_match', 'db_match'], name='gloader_fli_arc_mat_9188f9_idx'),
        ),
    ]
