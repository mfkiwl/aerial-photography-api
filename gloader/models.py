from django.contrib.gis.db import models

from aerial_data.models import *
# Create your models here.

GS_NSW = 1
GS_VIC = 2
GS_QLD = 3
GS_SA = 4
GS_WA = 5
GS_TAS = 6
GS_NT = 7
GS_ACT = 8
GS_OTHER = 9
GS_UNKNOWN = 10
GS_PNG = 11
GS_TERR = 12
GS_SAMOA = 13
GS_TONGA = 14

gstate_choices = [
    (GS_NSW, "NSW"),
    (GS_VIC, "VIC"),
    (GS_QLD, "QLD"),
    (GS_SA, "SA"),
    (GS_WA, "WA"),
    (GS_TAS, "TAS"),
    (GS_NT, "NT"),
    (GS_ACT, "ACT"),
    (GS_OTHER, "Other"),
    (GS_UNKNOWN, "Unknown"),
    (GS_PNG, "PNG"),
    (GS_TERR, "Australian Territory"),
    (GS_SAMOA, "Samoa"),
    (GS_TONGA, "Tonga"),
]
gstate_dict = dict(gstate_choices)
gstate_revdict = dict([ (b,a) for a,b in gstate_choices ])

gstate_map = {
    GS_NSW: S_NSW,
    GS_VIC: S_VIC,
    GS_QLD: S_QLD,
    GS_SA: S_SA,
    GS_WA: S_WA,
    GS_TAS: S_TAS,
    GS_NT: S_NT,
    GS_ACT: S_ACT,
    GS_OTHER: S_UNKNOWN,
    GS_UNKNOWN: S_UNKNOWN,
    GS_PNG: S_PNG,
    GS_TERR: S_TERR,
    GS_SAMOA: S_WS,
    GS_TONGA: S_TONGA,
}

GDT_UNKNOWN = 0
GDT_DEF_DMY = 1
GDT_DEF_MY  = 2
GDT_DEF_Y   = 3
GDT_NEEDS_CONFIRM = 4

gdt_choices = [
    (GDT_UNKNOWN, "Unknown"),
    (GDT_DEF_DMY, "Definite date"),
    (GDT_DEF_MY, "Definite month"),
    (GDT_DEF_Y, "Definite year"),
    (GDT_NEEDS_CONFIRM, "Needs confirmation"),
]
gdt_dict = dict(gdt_choices)

gdt_map = {
    GDT_UNKNOWN: UNKNOWN,
    GDT_DEF_DMY: DEFINITE_DMY,
    GDT_DEF_MY: DEFINITE_MY,
    GDT_DEF_Y: DEFINITE_Y,
    GDT_NEEDS_CONFIRM: NEEDS_CONFIRMATION,
}

GCC_UNKNOWN = 0
GCC_WILD = 1
GCC_WILLIAMSON = 2
GCC_FAIRCHILD = 3
GCC_HASSELBLAD = 4
GCC_HASS = 5
GCC_VINTEN = 6
GCC_ZEISS = 7

gcc_choices = [
    (GCC_UNKNOWN, "Unknown"),
    (GCC_WILD, "Wild"),
    (GCC_WILLIAMSON, "Williamson"),
    (GCC_FAIRCHILD, "Fairchild"),
    (GCC_HASSELBLAD, "Hasselblad"),
    (GCC_HASS, "Hass"),
    (GCC_VINTEN, "Vinten"),
    (GCC_ZEISS, "Zeiss"),
]

gcc_dict = dict(gcc_choices)

GCM_UNKNOWN = 0
GCM_RC8 = 1
GCM_RC9 = 2
GCM_RC10 = 3
GCM_K17 = 4
GCM_T11 = 5
GCM_T12 = 6
GCM_EAGLE_4 = 7
GCM_EAGLE_9 = 8
GCM_F24 = 9
GCM_F52 = 10
GCM_F8 = 11
GCM_F49 = 12
GCM_OSC = 13
GCM_RMK_A = 14
GCM_K18 = 15
GCM_RC30 = 16

gcm_choices = [
    (GCM_UNKNOWN, "Unknown"),
    (GCM_RC8, "RC8"),
    (GCM_RC9, "RC9"),
    (GCM_RC10, "RC10"),
    (GCM_K17, "K17"),
    (GCM_T11, "T11"),
    (GCM_T12, "T12"),
    (GCM_EAGLE_4, "Eagle 4"),
    (GCM_EAGLE_9, "Eagle 9"),
    (GCM_F24, "F24"),
    (GCM_F52, "F52"),
    (GCM_F8, "F8"),
    (GCM_F49, "F49"),
    (GCM_OSC, "O.S.C."),
    (GCM_RMK_A, "RMK-A"),
    (GCM_K18, "K18"),
    (GCM_RC30, "RC30"),
]
gcm_dict = dict(gcm_choices)

GSRC_UNKNOWN_NONE = 0
GSRC_FLD = 1
GSRC_CALC = 2
GSRC_UNKNOWN_MULT=3

gsrc_choices = [
    (GSRC_UNKNOWN_NONE, "Unknown - no information on flight line diagram"),
    (GSRC_FLD, "Sourced from flight line diagram"),
    (GSRC_CALC, "Calculated from other fields"),
    (GSRC_UNKNOWN_MULT, "Unknown - multiple values on flight line diagram"),
]
gsrc_dict = dict(gsrc_choices)

GFLU_UNKNOWN = 0
GFLU_MM = 1
GFLU_INCH = 2

gflu_choices = [
    (GFLU_UNKNOWN, "Unknown"),
    (GFLU_MM, "mm"),
    (GFLU_INCH, "inch"),
]
gflu_dict=dict(gflu_choices)

GHU_UNKNOWN = 0
GHU_METRES = 1
GHU_FEET = 2

ghu_choices = [
    (GHU_UNKNOWN, "Unknown"),
    (GHU_METRES, "metres"),
    (GHU_FEET, "feet"),
]
ghu_dict=dict(ghu_choices)

GFT_UNKNOWN = 0
GFT_BW = 1
GFT_COLOUR = 2
GFT_BW_IR = 3
GFT_COLOUR_IR = 4
GFT_IR = 5
GFT_OTHER = 6

gft_choices = [
    (GFT_UNKNOWN, "Unknown"),
    (GFT_BW, "Black/White"),
    (GFT_COLOUR, "Colour"),
    (GFT_BW_IR, "Black/White Infrared"),
    (GFT_COLOUR_IR, "Colour Infrared"),
    (GFT_IR, "Infrared"),
    (GFT_OTHER, "Other"),
]
gft_dict = dict(gft_choices)

GFF_UNKNOWN = 0
GFF_VERTICAL = 1
GFF_OBLIQUE = 2
GFF_OTHER = 3

gff_choices = [
    (GFF_UNKNOWN, "Unknown"),
    (GFF_VERTICAL, "Vertical"),
    (GFF_OBLIQUE, "Oblique"),
    (GFF_OTHER, "Other"),
]
gff_dict = dict(gff_choices)

GSC_NO = 0
GSC_YES = 1

gsc_choices = [
    (GSC_NO, "No"),
    (GSC_YES, "Yes"),
]
gsc_dict = dict(gsc_choices)

GFLS_FLD = 0
GFLS_INTERPOLATED = 1

gfls_choices = [
    (GFLS_FLD, "Digitised from flight line diagram"),
    (GFLS_INTERPOLATED, "Interpolated from digitised points"),
]
gfls_dict = dict(gfls_choices)

class FlightArc(models.Model):
    # GDB Attributes
    ## Mapsheets
    mapno_250k = models.CharField(max_length=100, blank=True, null=True)
    mapna_250k = models.CharField(max_length=254, blank=True, null=True)
    mapno_100k = models.CharField(max_length=100, blank=True, null=True)
    mapna_100k = models.CharField(max_length=254, blank=True, null=True)
    mapno_50k = models.CharField(max_length=100, blank=True, null=True)
    mapna_50k = models.CharField(max_length=254, blank=True, null=True)
    ##
    state = models.IntegerField(choices=gstate_choices, default=GS_UNKNOWN)
    organ = models.CharField(max_length=254, null=True, blank=True)
    flt_line_diagram = models.CharField(max_length=1024)
    ## Main Linking Identifiers
    film_number = models.CharField(max_length=50)
    run = models.CharField(max_length=50, null=True, blank=True)
    frame_start = models.CharField(max_length=20, null=True, blank=True)
    frame_end = models.CharField(max_length=20, null=True, blank=True)
    ## Dates
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    year_start = models.CharField(max_length=4)
    year_end = models.CharField(max_length=4)
    date_type = models.IntegerField(choices=gdt_choices, default=GDT_UNKNOWN)
    ##
    # FLT_LINE_LENGTH OFTReal
    ## CAMERA AND LENS
    camera_company = models.IntegerField(choices=gcc_choices, default=GCC_UNKNOWN)
    camera_model = models.IntegerField(choices=gcm_choices, default=GCM_UNKNOWN)
    camera_serial = models.CharField(max_length=50, null=True, blank=True)
    camera_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    camera_comment = models.CharField(max_length=254, null=True, blank=True)
    lens_info = models.CharField(max_length=50, null=True, blank=True)
    focal_leng = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    focal_leng_unit = models.IntegerField(choices=gflu_choices, default=GFLU_UNKNOWN)
    focal_leng_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Flight geometry
    ave_height = models.DecimalField(max_digits=9, decimal_places=2, null=True, blank=True)
    ave_height_unit = models.IntegerField(choices=ghu_choices, default=GHU_UNKNOWN)
    ave_height_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ave_scale = models.IntegerField(null=True, blank=True)
    ave_scale_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    overlap = models.IntegerField(null=True, blank=True)
    overlap_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Film
    film_type = models.IntegerField(choices=gft_choices, default=GFT_UNKNOWN)
    film_form = models.IntegerField(choices=gff_choices, default=GFF_UNKNOWN)
    film_barcode = models.CharField(max_length=40, null=True, blank=True)
    ## General Metadata
    scanned = models.IntegerField(choices=gsc_choices, default=GFF_UNKNOWN)
    scan_comment = models.CharField(max_length=254, null=True, blank=True)
    photo_series = models.CharField(max_length=100, null=True, blank=True)
    flt_source = models.IntegerField(choices=gfls_choices, default=GFLS_FLD)
    flt_diag_ga_old = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_ga_new = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_pms = models.CharField(max_length=254, null=True, blank=True)
    comments = models.CharField(max_length=254, null=True, blank=True)
    # REVISED_DATE OFTDateTime
    # REVISED_COMMENT OFTString
    # SHAPE_Length OFTRea
    ## Record IDs
    gfid = models.CharField(max_length=254, unique=True)
    fid = models.IntegerField(unique=True)

    # Matched record
    db_match = models.OneToOneField(FlightRun, blank=True, null=True, on_delete=models.SET_NULL)

    # The actual Geometry
    flight_path = models.MultiLineStringField(srid=3577, blank=True, null=True)

    def __str__(self):
        return "arc %s:%s" % (self.fid, self.gfid)

    class Meta:
        indexes = [
            models.Index(fields=["film_number", "run"])
        ]


ORTHOPHOTO_NO = 0
ORTHOPHOTO_YES_FRAME = 1
ORTHOPHOTO_YES_MOSAIC = 2
ORTHOPHOTO_YES_FRAME_AND_MOSAIC = 3

ortho_choices = [
    (ORTHOPHOTO_NO, "No"),
    (ORTHOPHOTO_YES_FRAME, "Yes, as individual frame"),
    (ORTHOPHOTO_YES_MOSAIC, "Yes, as part of a mosaic"),
    (ORTHOPHOTO_YES_FRAME_AND_MOSAIC, "Yes, as individual frame and part of a mosaic"),
]


class FlightPoint(models.Model):
    # GDB Attributes
    ## Mapsheets
    mapno_250k = models.CharField(max_length=100, blank=True, null=True)
    mapna_250k = models.CharField(max_length=254, blank=True, null=True)
    mapno_100k = models.CharField(max_length=100, blank=True, null=True)
    mapna_100k = models.CharField(max_length=254, blank=True, null=True)
    mapno_50k = models.CharField(max_length=100, blank=True, null=True)
    mapna_50k = models.CharField(max_length=254, blank=True, null=True)
    ##
    state = models.IntegerField(choices=gstate_choices, default=GS_UNKNOWN)
    organ = models.CharField(max_length=254, null=True, blank=True)
    flt_line_diagram = models.CharField(max_length=1024)
    ## Main Linking Identifiers
    film_number = models.CharField(max_length=50)
    run = models.CharField(max_length=50, null=True, blank=True)
    frame = models.CharField(max_length=20, null=True, blank=True)
    photo_id = models.CharField(max_length=100, null=True, blank=True)
    ## Dates
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    year_start = models.CharField(max_length=4)
    year_end = models.CharField(max_length=4)
    date_type = models.IntegerField(choices=gdt_choices, default=GDT_UNKNOWN)
    ##
    # FLT_LINE_LENGTH OFTReal
    ## CAMERA AND LENS
    camera_company = models.IntegerField(choices=gcc_choices, default=GCC_UNKNOWN)
    camera_model = models.IntegerField(choices=gcm_choices, default=GCM_UNKNOWN)
    camera_serial = models.CharField(max_length=50, null=True, blank=True)
    camera_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    camera_comment = models.CharField(max_length=254, null=True, blank=True)
    lens_info = models.CharField(max_length=50, null=True, blank=True)
    focal_leng = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    focal_leng_unit = models.IntegerField(choices=gflu_choices, default=GFLU_UNKNOWN)
    focal_leng_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Flight geometry
    ave_height = models.DecimalField(max_digits=9, decimal_places=2, null=True, blank=True)
    ave_height_unit = models.IntegerField(choices=ghu_choices, default=GHU_UNKNOWN)
    ave_height_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ave_scale = models.IntegerField(null=True, blank=True)
    ave_scale_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    overlap = models.IntegerField(null=True, blank=True)
    overlap_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Film
    film_type = models.IntegerField(choices=gft_choices, default=GFT_UNKNOWN)
    film_form = models.IntegerField(choices=gff_choices, default=GFF_UNKNOWN)
    film_barcode = models.CharField(max_length=40, null=True, blank=True)
    ## General Metadata
    scanned = models.IntegerField(choices=gsc_choices, default=GFF_UNKNOWN)
    scan_comment = models.CharField(max_length=254, null=True, blank=True)
    photo_series = models.CharField(max_length=100, null=True, blank=True)
    flt_diag_ga_old = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_ga_new = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_pms = models.CharField(max_length=254, null=True, blank=True)
    comments = models.CharField(max_length=254, null=True, blank=True)
    # Image specific fields
    orthophoto = models.IntegerField(choices=ortho_choices, default=ORTHOPHOTO_NO)
    point_source = models.IntegerField(choices=gfls_choices, default=GFLS_FLD)
    # REVISED_DATE OFTDateTime
    # REVISED_COMMENT OFTString
    # SHAPE_Length OFTRea
    ## Record IDs
    gfid = models.CharField(max_length=254, unique=True)
    fid = models.IntegerField()

    # Matched record
    db_match = models.OneToOneField(AerialImage, blank=True, null=True, on_delete=models.SET_NULL)
    arc_match = models.ForeignKey(FlightArc, blank=True, null=True, on_delete=models.SET_NULL)

    # The geometries
    centroid = models.PointField(srid=3577, blank=True, null=True)

    def __str__(self):
        return "point %s:%s" % (self.fid, self.gfid)
    class Meta:
        indexes = [
            models.Index(fields=["arc_match", "db_match"])
        ]


class FlightFootPrint(models.Model):
    # GDB Attributes
    ## Mapsheets
    mapno_250k = models.CharField(max_length=100, blank=True, null=True)
    mapna_250k = models.CharField(max_length=254, blank=True, null=True)
    mapno_100k = models.CharField(max_length=100, blank=True, null=True)
    mapna_100k = models.CharField(max_length=254, blank=True, null=True)
    mapno_50k = models.CharField(max_length=100, blank=True, null=True)
    mapna_50k = models.CharField(max_length=254, blank=True, null=True)
    ##
    state = models.IntegerField(choices=gstate_choices, default=GS_UNKNOWN)
    organ = models.CharField(max_length=254, null=True, blank=True)
    flt_line_diagram = models.CharField(max_length=1024)
    ## Main Linking Identifiers
    film_number = models.CharField(max_length=50)
    run = models.CharField(max_length=50, null=True, blank=True)
    frame = models.CharField(max_length=20, null=True, blank=True)
    photo_id = models.CharField(max_length=100, null=True, blank=True)
    ## Dates
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    year_start = models.CharField(max_length=4)
    year_end = models.CharField(max_length=4)
    date_type = models.IntegerField(choices=gdt_choices, default=GDT_UNKNOWN)
    ##
    # FLT_LINE_LENGTH OFTReal
    ## CAMERA AND LENS
    camera_company = models.IntegerField(choices=gcc_choices, default=GCC_UNKNOWN)
    camera_model = models.IntegerField(choices=gcm_choices, default=GCM_UNKNOWN)
    camera_serial = models.CharField(max_length=50, null=True, blank=True)
    camera_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    camera_comment = models.CharField(max_length=254, null=True, blank=True)
    lens_info = models.CharField(max_length=50, null=True, blank=True)
    focal_leng = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    focal_leng_unit = models.IntegerField(choices=gflu_choices, default=GFLU_UNKNOWN)
    focal_leng_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Flight geometry
    ave_height = models.DecimalField(max_digits=9, decimal_places=2, null=True, blank=True)
    ave_height_unit = models.IntegerField(choices=ghu_choices, default=GHU_UNKNOWN)
    ave_height_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ave_scale = models.IntegerField(null=True, blank=True)
    ave_scale_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    overlap = models.IntegerField(null=True, blank=True)
    overlap_source = models.IntegerField(choices=gsrc_choices, default=GSRC_UNKNOWN_NONE)
    ## Film
    film_type = models.IntegerField(choices=gft_choices, default=GFT_UNKNOWN)
    film_form = models.IntegerField(choices=gff_choices, default=GFF_UNKNOWN)
    film_barcode = models.CharField(max_length=40, null=True, blank=True)
    ## General Metadata
    scanned = models.IntegerField(choices=gsc_choices, default=GFF_UNKNOWN)
    scan_comment = models.CharField(max_length=254, null=True, blank=True)
    photo_series = models.CharField(max_length=100, null=True, blank=True)
    flt_source = models.IntegerField(choices=gfls_choices, default=GFLS_FLD)
    flt_diag_ga_old = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_ga_new = models.CharField(max_length=150, null=True, blank=True)
    flt_diag_pms = models.CharField(max_length=254, null=True, blank=True)
    comments = models.CharField(max_length=254, null=True, blank=True)
    # Image specific fields
    orthophoto = models.IntegerField(choices=ortho_choices, default=ORTHOPHOTO_NO)
    footprint_source = models.IntegerField(choices=gfls_choices, default=GFLS_FLD)
    # REVISED_DATE OFTDateTime
    # REVISED_COMMENT OFTString
    # SHAPE_Length OFTRea
    ## Record IDs
    gfid = models.CharField(max_length=254, unique=True)
    fid = models.IntegerField()

    # Matched record
    db_match = models.OneToOneField(AerialImage, blank=True, null=True, on_delete=models.SET_NULL)
    arc_match = models.ForeignKey(FlightArc, blank=True, null=True, on_delete=models.SET_NULL)

    # The geometries
    footprint = models.MultiPolygonField(srid=3577, blank=True, null=True)


    def __str__(self):
        return "ftprnt %s:%s" % (self.fid, self.gfid)

