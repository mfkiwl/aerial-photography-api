==================
GDB Loader/Matcher
==================

This document describes the behaviour of the ``import_gdb`` management command.

Import_GDB assumes that ``import_db`` has already been run
against the spreadsheet and consists of 3 loaders:

1. the arc loader
2. the point loader
3. the footprint loader

The various loaders can be individually activated or deactivated
with the `-p/--process` flag.  Default is to process all.

The activated loaders are run in the order above (arc first).

Layers are identified by their name containing the appropriate
identifier and are expected to contain the appropriate geometry:

1. "Arc" (multiline-string)
2. "Points" (point)
3. "Footprints" (multipolygon)

If a layer contains two identifiers (e.g. "ArcPoints") then
both loaders will attempt to run, although only the one that
matches the stored geometry type will succeed.

Layers whose name ends in a digit are ignored.

----------
Arc Loader
----------

1. Load into staging table.
---------------------------

For each arc in the GDB arc layer:

1. Check for an existing staging table record (lookup by
   GFID).
2. Copy all GDB fields to the staging table, either using
   the existing record or creating a new one.

2. Match against main database (spreadsheet data)
-------------------------------------------------

For each record in the staging table:

1. Attempt to find match by run and film number.
2. If multiple matches are found, try to match be frame_start
   and frame_end.

If a single match has been found, copy the geometry to the
database.  (For all other fields, the spreadsheet data is used.)

If no match could be found, or if multiple matches are found,
the GDB data will NOT be merged into the main database.

------------
Point Loader
------------

1. Load into staging table.
---------------------------

For each point in the GDB point layer:

1. Check for an existing staging table record (lookup by
   GFID).
2. Copy all GDB fields to the staging table, either using
   the existing record or creating a new one.  If frame number
   is None, replace with an empty string.

2. Match against main database (spreadsheet data)
-------------------------------------------------

(Note that `import_db` only creates flight line records,
it does not create records in the main database for individual
images.)

For each record in the staging table:

1. Find the staging table arc record and check if we found a
   main database flightpath match for it above.  If we didn't,
   skip on to the next record.
2. Check the main database image table for images that belong
   to the flight line record and with a matching frame number.
   If we find one, update the point coordinates.  If we don't
   create an image record.

If no match could be found at any stage above, the GDB data will
NOT be merged into the main database.

----------------
Footprint Loader
----------------

Essentially the same as the Point Loader.
