import csv
import os

from django.core.management.base import BaseCommand, CommandError

from aerial_data.management.commands.reset_hap_db import Command as HapResetCommand
from gloader.models import *


class Command(HapResetCommand):
    help ="""Resets loading tables."""

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]

        self.delete_records(FlightFootPrint)
        self.delete_records(FlightPoint)
        self.delete_records(FlightArc)


