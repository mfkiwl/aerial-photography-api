import datetime

from django.db.models import F
from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.gdal import DataSource
from aerial_data.models import *
from gloader.models import *


layer_loaders = [
    {
        "identifier": "Arc",
        "name": "Arc",
        "plural_name": "Flight Arcs",
        "expected_geom": "MultiLineString",
        "loader_method": "process_arc",
        "matcher_method": "match_arcs",
        "process_flag": "a",
        "model": FlightArc,
    },
    {
        "identifier": "Points",
        "name": "Point",
        "plural_name": "Points",
        "expected_geom": "Point",
        "loader_method": "process_point",
        "matcher_method": "match_points",
        "process_flag": "p",
        "model": FlightPoint,
    },
    {
        "identifier": "Footprints",
        "name": "Footprint",
        "plural_name": "Footprints",
        "expected_geom": "MultiPolygon",
        "loader_method": "process_footprint",
        "matcher_method": "match_footprints",
        "process_flag": "f",
        "model": FlightFootPrint,
    },
]

class Command(BaseCommand):
    help ="""Read an aerial imagery geodatabase and load/merge data into a database."""

    def add_arguments(self, parser):
        parser.add_argument("-p", "--process", action="store", default="fap", dest="process",
                            help="Which sections to process a (arcs), p (points) and/or f (footprints). Defaults to 'apf' (all).")
        parser.add_argument("-b", "--batch", type=int, action="store", default=1, dest="batch_size",
                            help="Write to database in chunks of this size.  Defaults to 1 (slow, but good for tracing issues).")
        parser.add_argument("gdb_directory", help="The name of the aerial imagery geodatabase (exported to gdb)")

    def handle(self, *args, **options):
        self.verbosity = options["verbosity"]
        self.gdb_dir   = options["gdb_directory"]
        self.process   = options["process"]
        self.batch_size = options["batch_size"]

        self.ds = DataSource(self.gdb_dir)

        self.errors = 0

        if self.verbosity > 1:
            self.stdout.write("Scanning layers...")
            for li in range(len(self.ds)):
                lyr = self.ds[li]
                self.stdout.write("Detected layer %s" % lyr.name)
            self.stdout.write("===================================")
        self.skip = True
        for ldr in layer_loaders:
            if ldr["process_flag"] in self.process:
                if self.verbosity > 1:
                    self.stdout.write(f"Checking for {ldr['name']} layers")
                for li in range(len(self.ds)):
                    lyr = self.ds[li]
                    if ldr["identifier"] in lyr.name:
                        if lyr.name[-1] in "0123456789":
                            self.stdout.write("Skipping dated layer %s" % lyr.name)
                            continue
                        self.load_layer(lyr, ldr)
                        if self.verbosity > 1:
                            self.stdout.write("Completed %s layer: %s" % (ldr["name"], lyr.name))

        if self.errors:
            self.stdout.write(self.style.ERROR("Errors occurred during gdb load"))
        else:
            if self.verbosity > 0:
                self.stdout.write(self.style.SUCCESS("GDB load successfully completed"))

    def load_layer(self, lyr, ldr):
        if self.verbosity > 1:
            self.stdout.write("Found a %s layer: %s" % (ldr["name"], lyr.name))
        if str(lyr.geom_type) != ldr["expected_geom"]:
            self.stdout.write(self.style.ERROR("In %s Layer %s, the geometry type is %s (expecting %s)" % (
                            ldr["name"], lyr.name,
                            str(lyr.geom_type), ldr["expected_geom"])))
            self.errors += 1
            return
        if ldr.get("loader_method") and hasattr(self, ldr["loader_method"]):
            # Loop over features
            fc = 0
            fl = 0
            create_batch = []
            gfid_check = set()
            for f in lyr:
                if f["GFID"] in gfid_check:
                    self.stdout.write(self.style.ERROR(f"GFID {f['GFID']} duplicated in {lyr.name} layer!"))
                gfid_check.add(f["GFID"])
                gobj = getattr(self, ldr["loader_method"])(f)
                if gobj:
                    fl += 1
                    if self.batch_size == 1 or gobj.id:
                        gobj.save()
                    else:
                        create_batch.append(gobj)
                        if len(create_batch) >= self.batch_size:
                            ldr["model"].objects.bulk_create(create_batch)
                            create_batch = []
                fc += 1
            if self.verbosity > 0:
                self.stdout.write("%d %s read, %d successfully loaded" % (fc, ldr["plural_name"], fl))
        else:
            self.stdout.write(self.style.WARNING("%s layers not supported yet - skipping %s" % (ldr["name"], lyr.name)))

        if ldr.get("matcher_method") and hasattr(self, ldr["matcher_method"]):
            getattr(self, ldr["matcher_method"])()

    def process_arc(self, f):
        idx = f.fid
        try:
            fa = FlightArc.objects.get(gfid=f["GFID"])
        except FlightArc.DoesNotExist:
            fa = FlightArc(gfid=f["GFID"])
        fa.fid = idx
        for fld in f.fields:
            lfld = fld.lower()
            if hasattr(fa, lfld):
                if lfld in ("date_start", "date_end"):
                    if f[fld].value is not None:
                        ds = str(f[fld]).split(" ")[0]
                        d = datetime.datetime.strptime(ds, "%Y-%m-%d").date()
                        setattr(fa, lfld, d)
                elif lfld == "flt_source":
                   if f[fld].value == ' ':
                       setattr(fa, lfld, GFLS_FLD)
                   else:
                       setattr(fa, lfld, f[fld].value)
                elif lfld == "state":
                    try:
                        ival = int(f[fld].value)
                        setattr(fa, lfld, f[fld].value)
                    except ValueError:
                        val = f[fld].value
                        if val in gstate_revdict:
                            setattr(fa, lfld, gstate_revdict[val])
                            self.stdout.write(self.style.WARNING("%s: state value %s is not an integer (fixed)" % (fa, val)))
                        else:
                            self.stdout.write(self.style.ERROR("%s: state value %s is not an integer (cannot fix)" % (fa, val)))
                else:
                    if f[fld].value is not None and f[fld].value != '':
                        setattr(fa, lfld, f[fld].value)
        fa.flight_path = f.geom.geos
        return fa

    def match_arcs(self):
        total_arcs = FlightArc.objects.count()
        arcs_matched_this_run = 0
        for fa in FlightArc.objects.filter(db_match__isnull=True):
            fr = self.match_fr_for_arc(fa)
            if fr:
                try:
                    fa.db_match = fr
                    fa.save()
                    fr.flight_path = fa.flight_path
                    fr.save()
                    arcs_matched_this_run += 1
                except Exception as e:
                    fad = FlightArc.objects.get(db_match=fr)
                    self.stdout.write(self.style.ERROR("Two GDB records match spreadsheet line %d: %s and %s" % (
                            fr.spreadsheet_line,
                            str(fad),
                            str(fa),
                    )))

        total_matched = FlightArc.objects.filter(db_match__isnull=False).count()
        if self.verbosity > 0:
            self.stdout.write(self.style.SUCCESS("Matched %d more arcs. Cumulative match rate %d/%d (%.2f%%)" % (
                    arcs_matched_this_run,
                    total_matched,
                    total_arcs,
                    (total_matched*100.0/total_arcs)
            )))
        return

    def match_fr_for_arc(self, fa):
        frs = FlightRun.objects.filter(run=fa.run, film__number=fa.film_number).exclude(total_photos__startswith="SAME ")
        if frs.count() == 0:
            self.stdout.write(self.style.ERROR("%s No matching spreadsheet record found (film %s run %s)" % (
                str(fa), fa.film_number, fa.run,
            )))
            self.errors += 1
            return None
        if frs.count() == 1:
            for fr in frs:
                return fr

        candidate_records = ",".join([ str(f.spreadsheet_line) for f in frs ])
        if self.verbosity > 2:
            self.stdout.write("Arc %d has %d regular db matches (xls %s) - CONTINUING" %(
                fa.fid, frs.count(), candidate_records
            ))

        if fa.frame_start is None:
            frs = frs.filter(start_frame__isnull=True)
        else:
            frs = frs.filter(start_frame__gte=fa.frame_start)
        if fa.frame_end is None:
            frs = frs.filter(end_frame__isnull=True)
        else:
            frs = frs.filter(end_frame__gte=fa.frame_end)

        if frs.count() == 0:
            self.stdout.write(self.style.ERROR("%s Could NOT find matching spreadsheet record: film %s run %s [frames %s-%s] (candidates before frames: %s)" % (
                str(fa), fa.film_number, fa.run, fa.frame_start, fa.frame_end, candidate_records
            )))
            self.errors += 1
            return None
        if frs.count() == 1:
            for fr in frs:
                return fr

        self.stdout.write("%s (film %s run %s frames %s-%s) has %d spreadsheet matches (lines %s) AFTER frame matching by frame" % (
            str(fa), fa.film_number, fa.run, fa.frame_start, fa.frame_end, frs.count(), ",".join([ str(f.spreadsheet_line) for f in frs ])
        ))

        return None

    def process_point(self, f):
        try:
            fp = FlightPoint.objects.get(gfid=f["GFID"])
        except FlightPoint.DoesNotExist:
            fp = FlightPoint(gfid=f["GFID"])
        self.process_point_or_footprint(f, fp)
        fp.centroid = f.geom.geos
        return fp

    def process_footprint(self, f):
        try:
            fp = FlightFootPrint.objects.get(gfid=f["GFID"])
        except FlightFootPrint.DoesNotExist:
            fp = FlightFootPrint(gfid=f["GFID"])
        self.process_point_or_footprint(f, fp)
        fp.footprint = f.geom.geos
        return fp

    def process_point_or_footprint(self, f, fp):
        fp.fid = f.fid
        for fld in f.fields:
            lfld = fld.lower()
            if hasattr(fp, lfld):
                if lfld in ("date_start", "date_end"):
                    if f[fld].value is not None:
                        ds = str(f[fld]).split(" ")[0]
                        d = datetime.datetime.strptime(ds, "%Y-%m-%d").date()
                        setattr(fp, lfld, d)
                elif lfld == "state":
                    try:
                        ival = int(f[fld].value)
                        setattr(fp, lfld, f[fld].value)
                    except ValueError:
                        val = f[fld].value
                        if val in gstate_revdict:
                            setattr(fp, lfld, gstate_revdict[val])
                            self.stdout.write(self.style.WARNING("%s: state value %s is not an integer (fixed)" % (fp,val)))
                        else:
                            self.stdout.write(self.style.ERROR("%s: state value %s is not an integer (cannot fix)" % (fp, val)))
                elif lfld == "point_source":
                    if f[fld].value == ' ':
                        setattr(fp, lfld, GFLS_FLD)
                    else:
                        setattr(fp, lfld, f[fld].value)
                elif lfld == "frame":
                    frame = f[fld].value
                    if frame is not None:
                        fp.frame = frame.strip()
                    else:
                        self.stdout.write(self.style.WARNING(f"{fp}: null frame - substituting empty string"))
                        fp.frame = ""
                else:
                    if f[fld].value is not None and f[fld].value != '':
                        setattr(fp, lfld, f[fld].value)
            elif lfld == "flt_arc_id":
                arc_id = f[fld].value
                if arc_id or arc_id == "no arc":
                    try:
                        if arc_id.startswith("{"):
                            fa = FlightArc.objects.get(gfid=arc_id)
                        else:
                            fa = FlightArc.objects.get(gfid="{%s}" % arc_id)
                        fp.arc_match = fa
                    except FlightArc.DoesNotExist:
                        self.stdout.write(self.style.ERROR("%s: arc %s does not exist" % (fp, arc_id)))
                else:
                    self.stdout.write(self.style.ERROR("%s: No arc id" % fp))

    def match_points(self):
        self.stdout.write("Matching points to db images")

        for fp in FlightPoint.objects.filter(arc_match__db_match__isnull=False, db_match__isnull=True):
            ai = self.match_ai_for_point(fp)
            if ai:
                try:
                    fp.db_match = ai
                    fp.save()
                    ai.centroid = fp.centroid
                    ai.save()
                except Exception as e:
                    fpd = FlightPoint.objects.get(db_match=ai)
                    self.stdout.write(self.style.ERROR("Two GDB point records match aerial image id %d: %s and %s" % (
                        ai.id,
                        fpd,
                        fp
                    )))

        return

    def match_footprints(self):
        self.stdout.write("Matching to footprints to db images")

        FlightFootPrint.objects.filter(arc_match__db_match__isnull=False, db_match__isnull=True).count()
        for fp in FlightFootPrint.objects.filter(arc_match__db_match__isnull=False, db_match__isnull=True):
            ai = self.match_ai_for_point(fp)
            if ai:
                try:
                    fp.db_match = ai
                    fp.save()
                    ai.footprint = fp.footprint
                    ai.save()
                except Exception as e:
                    fpd = FlightFootPrint.objects.get(db_match=ai)
                    self.stdout.write(self.style.ERROR("Two GDB footprint records match aerial image id %d: %s and %s" % (
                        ai.id,
                        fpd,
                        fp
                    )))

        return

    def match_ai_for_point(self, fp):
        if fp.frame is None:
            self.stdout.write(self.style.ERROR("Point record %d has no frame number." % fp.fid))
            return None
        ai_candidates = AerialImage.objects.filter(flight_run=fp.arc_match.db_match)
        ai_candidates = ai_candidates.filter(frame_number=fp.frame)
        if ai_candidates.count() > 0:
            return ai_candidates.first()

        # So create one.
        if fp.arc_match.db_match.film_meta.film_digitised == FILM_DIG_NOT:
            ortho = IMAGE_TYPE_UNDIGITISED
        elif fp.orthophoto == ORTHOPHOTO_NO:
            ortho = IMAGE_TYPE_RAW
        else:
            ortho = IMAGE_TYPE_GEOREF
        return fp.arc_match.db_match.create_image(fp.frame, ortho)